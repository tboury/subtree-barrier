from utilitary import rearrange_sequences
import random

LOC_DEBUG = 0

def desintricate(s1, s2, RNA_s, ss1, ss2):
    """
    Create two sequences geometrically equivalent to s1 and s2 (and make evolve th corresponding RNA instance RNA_s) by removing 
    intrication of secondary structures, that is to say by deplacing parenthesis and creating new position when two parentheses were
    opposite at a given position in s1 versus s2
    """
    new_s1 = ""
    new_s2 = ""
    new_RNA_s = ""

    for i in range(len(s1)):
        if ((s1[i] == '(') and (s2[i] == ')')):
            new_s1+= "(."
            new_s2+= ".)"
            new_RNA_s += RNA_s[i]*2
        elif ((s1[i] == ')') and (s2[i] == '(')):
            new_s1+= ".)"
            new_s2+= "(."
            new_RNA_s += RNA_s[i]*2
        elif ((s1[i] == ')') and (s2[i] == ')')):
            new_RNA_s += RNA_s[i]*2
            if ss1[i] < ss2[i]:
                new_s1+= ")."
                new_s2+= ".)"
            else:
                new_s1+= ".)"
                new_s2+= ")."
        elif ((s1[i] == '(') and (s2[i] == '(')):
            new_RNA_s += RNA_s[i]*2
            if ss1[i] < ss2[i]:
                new_s1+= "(."
                new_s2+= ".("
            else:
                new_s1+= ".("
                new_s2+= "(."
        else:
            new_s1+=(s1[i])
            new_s2+=(s2[i])
            new_RNA_s += RNA_s[i]
    return (new_s1, new_s2, new_RNA_s)

def core_unfold_weight(s1, s2, RNA_s):
    """
    Replace parenthesis in s1 and s2 by multiple once depending of if the secondary structure is "GC" (weight 3), "AU" (weight 2) or "GU" (weight 1)
    """
    (s1,s2,ss1,ss2,bps1,bps2) = rearrange_sequences(s1, s2, 0)
    new_s1 = ""
    new_s2 = ""
    for i in range(len(s1)):
        if ss1[i] != -1:
            if ((RNA_s[i] == 'G') and (RNA_s[ss1[i]]  == 'C')) or ((RNA_s[i] == 'C') and (RNA_s[ss1[i]]  == 'G')):
                new_s1+= s1[i]*21
                new_s2+= "."*21
            elif ((RNA_s[i] == 'A') and (RNA_s[ss1[i]]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[ss1[i]]  == 'A')):
                new_s1+= s1[i]*5
                new_s2+= "."*5
            else:#if ((RNA_s[i] == 'G') and (RNA_s[ss1[i]]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[ss1[i]] == 'G')):
                new_s1+= s1[i]*9
                new_s2+= "."*9
        elif ss2[i] != -1:
            if ((RNA_s[i] == 'G') and (RNA_s[ss2[i]]  == 'C')) or ((RNA_s[i] == 'C') and (RNA_s[ss2[i]]  == 'G')):
                new_s2+= s2[i]*21
                new_s1+= "."*21
            elif ((RNA_s[i] == 'A') and (RNA_s[ss2[i]]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[ss2[i]]  == 'A')):
                new_s2+= s2[i]*5
                new_s1+= "."*5
            else:#if ((RNA_s[i] == 'G') and (RNA_s[ss2[i]]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[ss2[i]] == 'G')):
                new_s2+= s2[i]*9
                new_s1+= "."*9
    return rearrange_sequences(new_s1, new_s2, 0)

def unfold_weight(s1, s2, RNA_s, ss1, ss2):
    """
    Unfold weighted nodes by replacing with multiples once. It is done for methods that do not support weighted method natively
    """
    (s1, s2, RNA_s) = desintricate(s1, s2, RNA_s, ss1, ss2)
    if LOC_DEBUG:
        print("After desintrication: s1", s1, "\ns2", s2, "\nRNA_s", RNA_s)
    return core_unfold_weight(s1, s2, RNA_s)

def give_weight(s1, s2, RNA_s, bps1, bps2):
    """
    Give weight to each arc detected in s1 and s2 for the subtree method according to the chain of nucleotides RNA_s
    """
    weight = [-1]
    li_bps1 = list(bps1)
    li_bps1.sort()
    li_bps2 = list(bps2)
    li_bps2.sort()
    
    for (i,j) in li_bps1:
        if ((RNA_s[i] == 'G') and (RNA_s[j]  == 'C')) or ((RNA_s[i] == 'C') and (RNA_s[j]  == 'G')):
            weight.append(21)
        elif ((RNA_s[i] == 'A') and (RNA_s[j]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[j]  == 'A')):
            weight.append(5)
        elif ((RNA_s[i] == 'G') and (RNA_s[j]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[j]  == 'G')):
            weight.append(9)
    for (i,j) in li_bps2:
        if ((RNA_s[i] == 'G') and (RNA_s[j]  == 'C')) or ((RNA_s[i] == 'C') and (RNA_s[j]  == 'G')):
            weight.append(21)
        elif ((RNA_s[i] == 'A') and (RNA_s[j]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[j]  == 'A')):
            weight.append(5)
        elif ((RNA_s[i] == 'G') and (RNA_s[j]  == 'U')) or ((RNA_s[i] == 'U') and (RNA_s[j]  == 'G')):
            weight.append(9)
    return weight

def build_RNA(ss1, ss2):
    """
    Build the RNA sequence of nucleotids compatible for sets of parentheses induced by ss1 and ss2
    """
    stock = ["A", "U", "C", "G"]
    RNA_s = ["" for i in range(len(ss1))]
    (list_paths, all_cycles) = build_path_cycles(ss1, ss2)
    for path in list_paths:
        RNA_s = weight_for_path(path, RNA_s)
        if LOC_DEBUG:
            print("RNA path after ", path, " gives: ", RNA_s )
    for cycle in all_cycles:
        RNA_s = weight_for_cycle(cycle, RNA_s)
        if LOC_DEBUG:
            print("RNA cycle after ", cycle, " gives: ", RNA_s )
    for index in range(len(RNA_s)):
        if RNA_s[index] == "":
            num = random.randint(0,3)
            RNA_s[index] = stock[num]
    if LOC_DEBUG:
        print("RNA build finished", RNA_s )
    return RNA_s



    

def build_path_cycles(ss1, ss2):
    """
        Indicates the list of the paths of secondary structures and the list of the cycles of secondary structures inside instances ss1 and ss2
    """
    list_paths = []
    all_cycles = []
    consume = [-1 for i in range(len(ss1))]
    for i in range(len(ss1)):
        iscycle = 0
        if consume[i] == -1:
            res_blue = [i]
            ss = ss1
            ssbis = ss2
            j = i
            while ss[j] not in res_blue and ss[j] != -1:
                res_blue+= [ss[j]]
                j = ss[j]
                ss, ssbis = ssbis, ss
           #list_paths.append(res_blue)
            if ss[j] in res_blue:
                ind = res_blue.index(ss[j])
                for num in res_blue[ind:]:
                    consume[num] = 1
                if i in res_blue[ind:]:
                    iscycle = 1
                all_cycles.append(res_blue[ind:])
                res_blue = res_blue[:ind]
            #print("blue", res_blue)
            res_red = [i]
            ss = ss2
            ssbis = ss1
            j = i
            while ss[j] not in res_red and ss[j] != -1:
                res_red+= [ss[j]]
                j= ss[j]
                #print("hola")
                ss, ssbis = ssbis, ss
            if (ss[j] in res_red):
                ind = res_red.index(ss[j])
                if iscycle == 0:
                    for num in res_red[ind:]:
                        consume[num] = 1
                    all_cycles.append(res_red[ind:])
                res_red = res_red[:ind]
            #print("red", res_red)
            res_red = res_red[1:]
            res_red.reverse()
            res_tot = res_red + res_blue
            for num in res_tot:
                consume[num] = 1
            if len(res_tot) > 1:
                list_paths.append(res_tot)
    return list_paths, all_cycles


def prob(list_proba):
    """
        Given a list_proba seen as a "stotastic vector" returns the letter associated to the random index drawn according to the stochastic vector list_proba
    """
    lettrine = ["A", "U", "C", "G"]
    number = random.random()
    sommation = 0
    for ind in range(len(list_proba)):
        sommation+= list_proba[ind]
        if number <= sommation:
            return lettrine[ind]
fibo = [0,1]

def build_fibo(n):
    k=2
    while (k <= n):
        fibo.append(fibo[k - 1] + fibo[k - 2])
        k = k + 1
build_fibo(3000) 

def weight_for_path(path, RNA_list):
    """
        Launch the automata that serves to gives letter to each position of a path, put these letters at the correct position in RNA_list and returns the latter
    """
    n  = len(path)
    lettrine = ["A", "U", "C", "G"]
    c = 2 * fibo[n + 2]
    p_start = [fibo[n]/c, fibo[n + 1]/c, fibo[n]/c, fibo[n + 1]/c]
    fromA = [0, 1, 0, 0]
    fromU = [0.5, 0, 0, 0.5]
    fromG = [0, 0.5, 0.5, 0]
    fromC = [0, 0, 0, 1]
    fromALL = [fromA, fromU, fromC, fromG]
    if path != []:
        RNA_list[path[0]] = prob(p_start)
        k = 1
    for index in range(1,len(path)):
        c = fibo[n + 2 - k]
        fromU = [fibo[n -k]/c, 0, 0, fibo[n -k + 1]/c]
        fromG = [ 0, fibo[n -k + 1]/c, fibo[n -k]/c, 0]
        fromALL = [fromA, fromU, fromC, fromG]
        letter = RNA_list[path[index-1]]
        id_from = lettrine.index(letter)
        from_loc = fromALL[id_from]
        RNA_list[path[index]] = prob(from_loc) 
        k = k + 1
    return RNA_list

#build_RNA([-1, 3, -1, 1, 7, 6, 5, 4,9,8,11,10,13,12, -1], [12, 6, 5, 4, 3, 2, 1, -1, 11, 10, 9, 8, 0, -1, -1])
def weight_for_cycle(cycle, RNA_list):
    """
        Launch the automata that serves to gives letter to each position of a cycle, put these letters at the correct position in RNA_list and returns the latter
    """
    n  = len(cycle)
    lettrine = ["A", "U", "C", "G"]
    c = 2 * (fibo[n - 1] + fibo[n + 1])
    p_start = [fibo[n - 1]/c, fibo[n + 1]/c, fibo[n - 1]/c, fibo[n + 1]/c]
    if cycle != []:
        RNA_list[cycle[0]] = prob(p_start)
        if (RNA_list[cycle[0]] == "A") or (RNA_list[cycle[0]] == "C"):
            if len(cycle) > 1:
                if RNA_list[cycle[0]] == "A":
                    RNA_list[cycle[1]] = "U"
                else:
                    RNA_list[cycle[1]] = "G"
                RNA_list = weight_almost_path(cycle[1:], RNA_list, n - 1)
        else:
            RNA_list = weight_almost_path(cycle, RNA_list, n + 1)
    return RNA_list

def weight_almost_path(path, RNA_list, n):
    """
        When first letters were decided for cycle, automata for cycle works almost as the automata for path
    """
    lettrine = ["A", "U", "C", "G"]
    fromA = [0, 1, 0, 0]
    fromU = [0.5, 0, 0, 0.5]
    fromG = [0, 0.5, 0.5, 0]
    fromC = [0, 0, 0, 1]
    fromALL = [fromA, fromU, fromC, fromG]
    k = 0
    for index in range(1,len(path)):
        c = fibo[n - k]
        fromU = [fibo[n -k - 2]/c, 0, 0, fibo[n -k - 1]/c]
        fromG = [ 0, fibo[n -k -1]/c, fibo[n -k - 2]/c, 0]
        fromALL = [fromA, fromU, fromC, fromG]
        letter = RNA_list[path[index-1]]
        id_from = lettrine.index(letter)
        from_loc = fromALL[id_from]
        RNA_list[path[index]] = prob(from_loc) 
        k = k + 1
    return RNA_list

#LITTLE TESTS

#s1_subtree .(((..))).
#s2_subtree .(.).(..).

#desintricate("((.))",".(..)", "UGGUU", "AUAAU",[4, 3, -1, 1, 0], [-1, 4, -1, -1, 1])

#core_unfold_weight("..(.(.(...).))", 
#                   "((.(.(.))).)..", 
#                   "UGUGGTAGTU", "GATUGTUGTA")

#build_path_cycles([-1, 8, 7, 6, -1, -1, 3, 2, 1, -1], [-1, 3, -1, 1, -1, 8, -1, -1, 5, -1])

#The sequences tested below
#s1 .(.)(())()()()((.())).
#s2 (((())).(()))(.(.)().)
#build_path_cycles([-1, 3, -1, 1, 7, 6, 5, 4 , 9, 8 , 11, 10, 13, 12, 20, 19, -1, 18, 17, 15, 14, -1], 
#                  [12, 6, 5, 4, 3, 2, 1, -1, 11, 10, 9, 8, 0, 21, -1, 17, -1, 15, 19, 18, -1, 13])



#build_RNA([-1, 3, -1, 1, 7, 6, 5, 4 , 9, 8 , 11, 10, 13, 12, 20, 19, -1, 18, 17, 15, 14, -1], 
#                  [12, 6, 5, 4, 3, 2, 1, -1, 11, 10, 9, 8, 0, 21, -1, 17, -1, 15, 19, 18, -1, 13])
#random.seed(2021)
#build_RNA([-1, 7, 4, -1, 2, -1, -1, 1, -1, -1], [-1, 8, 7, 6, -1, -1, 3, 2, 1, -1])
#build_RNA([-1, 7, 4, -1, 2, -1, -1, 1, -1, -1], [-1, 8, 7, 6, -1, -1, 3, 2, 1, -1])