import sys
import time
import multiprocessing
import matplotlib.pylab as plt
import os
import csv
import random 
from decimal import Decimal 
from rna_interface import random_conflict_graph, structures_to_conflict_graph
from dpw_interface import extend_to_pm
from maximum_matching import maximum_matching
from dpw_interface import construct_digraph
from barriers_py3 import realize
from dpw_algorithm import DPW
from graph_classes import Digraph

from weighted_ordering import main_subtree, init_tree_circle_formalism, count_leafs
from utilitary import build_local_stats, build_total_stats, initialize_sequences, rearrange_sequences
#from configuration_tests import choose_parameter
from weighter import unfold_weight, give_weight, build_RNA
seed = 2021

def update(s1, bps1, bps2, arc):
    if arc < len(bps1):
        (num1, num2) =bps1[arc] 
        s1 = list(s1)
        s1[num1] = '.'
        s1[num2] = '.'
        s1 = "".join(s1)
    else:
        (num1, num2) =bps2[arc - len(bps1)] 
        s1 = list(s1)
        s1[num1] = '('
        s1[num2] = ')'
        s1= "".join(s1)
    return s1


def generate():
    n = 60
    total = []
    total.append(["s1", "s2", "RNA_seq", "K_unweighted", "Turner_unweighted","max_Turner_unweight", "K_weighted", "Turner_weighted","max_Turner_weight"])
    victory = 0
    lose = 0
    neutral = 0
    seed=2021
    for i in range(100):
        print("iteration :", (seed -2020))
        random.seed(seed) 
        (s1,s2,ss1,ss2,bps1,bps2) = initialize_sequences(n, seed, 3, 5., 7., 0, additional = [7., 3, 10.], count_fixed = 1)
        s1_old = s1
        print("s1_subtree", s1)
        print("s2_subtree", s2)
        bps1 = list(bps1)
        bps2 = list(bps2)
        bps1.sort()
        bps2.sort()
        bps1 = [(-1, -1)] +bps1

        
        for j in range(1): 
            s1 = s1_old
            RNA_s = build_RNA(ss1,ss2)
            print("RNA_s", RNA_s)
            weight = give_weight(s1, s2, RNA_s, bps1, bps2)
            (schedule_weight, k_weight) = main_subtree(s1,s2, weight = weight)
            (schedule, k) = main_subtree(s1,s2)
            fichier = open("tester.txt", "w")
            fichier.write("".join(RNA_s))
            fichier.write("\n")
            fichier.write(s1)
            fichier.write("\n")
            while schedule:
                fichier.write("".join(RNA_s))
                fichier.write("\n")
                s1 = update(s1, bps1, bps2, schedule[0])
                fichier.write(s1)
                fichier.write("\n")
                schedule = schedule[1:]
            fichier.close()
            os.system("RNAeval tester.txt > output.txt")
            fichier = open("output.txt", "r")
            file = fichier.readlines()
            list_barrier = []
            for index in range(1, len(file),2):
                line = file[index]
                line = line[n + 3: -2]
                num = Decimal(line)
                list_barrier.append(num)
            maxi = max(list_barrier[0], list_barrier[-1])
            max_barrier_unweight = 0
            max_Turner_unweight = 0
            for E in list_barrier:
                max_Turner_unweight = max(max_Turner_unweight,E)
                max_barrier_unweight = max(max_barrier_unweight, E - maxi) 
            fichier.close()
            resu = [s1_old, s2, "".join(RNA_s), k, float(max_barrier_unweight), float(max_Turner_unweight)]
            s1 = s1_old
            fichier = open("tester.txt", "w")
            fichier.write("".join(RNA_s))
            fichier.write("\n")
            fichier.write(s1)
            fichier.write("\n")
            while schedule_weight:
                fichier.write("".join(RNA_s))
                fichier.write("\n")
                s1 = update(s1, bps1, bps2, schedule_weight[0])
                fichier.write(s1)
                fichier.write("\n")
                schedule_weight = schedule_weight[1:]
            fichier.close()
            os.system("RNAeval tester.txt > output.txt")
            fichier = open("output.txt", "r")
            file = fichier.readlines()
            list_barrier = []
            for index in range(1, len(file),2):
                line = file[index]
                line = line[n + 3: -2]
                num = Decimal(line)
                list_barrier.append(num)
            maxi = max(list_barrier[0], list_barrier[-1])
            max_barrier = 0
            max_Turner_weight = 0
            for E in list_barrier:
                max_Turner_weight = max(max_Turner_weight,E)
                max_barrier = max(max_barrier, E - maxi) 
            fichier.close()
            resu += [k_weight, float(max_barrier), float(max_Turner_weight)]
            if max_barrier  < max_barrier_unweight:
                victory += 1
            elif max_barrier > max_barrier_unweight:
                lose += 1
            else:
                neutral+=1
            total.append(resu)
            seed+=1
    with open('Turner_vs_weighted_vs_simple.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(total)
    print("strict positive case", victory, "losing case", lose, "neutral_case", neutral)
generate()



