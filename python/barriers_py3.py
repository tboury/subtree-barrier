# v3: skip includes and blacklists
# v3b lazy eval after separator
# best so far

#import svglib
#import svgwrite
import random
import sys
import os
import getopt
import time

## Parameters for random generation ##
MULTIPLE_HELIX_LENGTH = 1

DEBUG = False
SMALLDEBUG = True
LEAVEINCLUDES = True
LEAVEBLACKLIST = True
RUNBRUTEFORCE = False
TIME = False


maxk = 0
############# Basic utilities #################


def boolTuples(dim):
    if dim <= 0:
        return [()]
    else:
        res = []
        for t in boolTuples(dim-1):
            for b in [True, False]:
                res.append(tuple([b]+list(t)))
        return res


############# RNA structure stuffs #################
# Mostly "house-keeping" functions (parsing, conversions,
# uniform random generation...)

def ssparse(seq):
    """
    from well-parenthesized to table with -1 for unpaired and ( or )
    otherwise
    """
    res = [-1 for c in seq]
    p = []
    for i, c in enumerate(seq):
        if c == '(':
            p.append(i)
        elif c == ')':
            j = p.pop()
            (res[i], res[j]) = (j, i)
    return res


def ssstring(ss):
    """
    from table with -1 on unpaired position and ( ) elsewhere
    to well-parenthesized string
    """
    res = ["." for i in ss]
    for i in range(len(ss)):
        if ss[i] == -1:
            pass
        elif ss[i] > i:
            res[i] = '('
        elif ss[i] < i:
            res[i] = ')'
    return "".join(res)


def sstopairs(ss):
    """
    well-parenthesized string to list of base pairs
    """
    if ss is None:
        return None
    res = []
    for i in range(len(ss)):
        if ss[i] > i:
            j = ss[i]
            res.append((i, j))
    return set(res)


def pairstoss(bps, n):
    res = [-1 for i in range(n)]
    for (i, j) in bps:
        res[i], res[j] = j, i
    return res


def sscount(n, is_nested, count, theta=1, unpaired_weight=0.1, leaf_weight=1.):
    if (n,is_nested) not in count:
        if n <= 0:
            if is_nested:
                count[(n,True)] = leaf_weight
            else:
                count[(n,False)] = 1.
        else:
            count[(n,is_nested)] = unpaired_weight*sscount(n-1,is_nested, count, theta=theta, unpaired_weight=unpaired_weight, leaf_weight=leaf_weight)
            for i in range(theta+2, n+1):
                count[(n,is_nested)] += sscount(i-2, True, count,theta=theta, unpaired_weight=unpaired_weight, leaf_weight=leaf_weight)*sscount(n-i, False, count, theta=theta, unpaired_weight=unpaired_weight, leaf_weight=leaf_weight)
    return count[(n,is_nested)]


# Taille 50

BASE_PAIRS = set([("A", "U"), ("U", "A"), ("G", "U"),
                  ("U", "G"), ("C", "G"), ("G", "C")])


def fillmatrix(seq, theta, taboo):
    n = len(seq)
    tab = [[-10000 for i in range(n)] for j in range(n)]
    for m in range(1, theta+2):
        for i in range(0, n-m+1):
            j = i + m-1
            tab[i][j] = 0
    for m in range(theta+2, n+1):
        for i in range(0, n-m+1):
            j = i + m-1
            tab[i][j] = tab[i+1][j]
            for k in range(i+theta+1, j+1):
                if ((i, k) not in taboo) and (seq[i], seq[k]) in BASE_PAIRS:
                    t2 = 0
                    if (k < j):
                        t2 = tab[k+1][j]
                    tab[i][j] = max(tab[i][j], 1+tab[i+1][k-1]+t2)
    return tab


def backtrack(seq, i, j, tab, theta, taboo):
    n = len(seq)
    m = j-i+1
    if m < theta+2:
        return []
    else:
        if tab[i][j] == tab[i+1][j]:
            return backtrack(seq, i+1, j, tab, theta, taboo)
        for k in range(i+theta+1, j+1):
            if ((i, k) not in taboo) and (seq[i], seq[k]) in BASE_PAIRS:
                t2 = 0
                if (k < j):
                    t2 = tab[k+1][j]
                if tab[i][j] == 1+tab[i+1][k-1]+t2:
                    return [(i, k)] + backtrack(seq, i+1, k-1, tab, theta, taboo) + backtrack(seq, k+1, j, tab, theta, taboo)
    raise Exception


def randomUniformSeq(n, seed=None):
    if seed:
        random.seed(seed)
    return "".join([random.choice(['A', 'C', 'G', 'U']) for i in range(n)])


def randomDistantStructPairs(n, theta=3, seed=None):
    rseq = randomUniformSeq(n, seed=seed)
    taboo = set()
    tab1 = fillmatrix(rseq, theta, taboo)
    bps1 = backtrack(rseq, 0, n-1, tab1, theta, taboo)
    taboo = set(bps1)
    tab2 = fillmatrix(rseq, theta, taboo)
    bps2 = backtrack(rseq, 0, n-1, tab2, theta, taboo)
    return (rseq, pairstoss(bps1, n), pairstoss(bps2, n))


def ssrandom(n, is_nested, count, seed=None, theta=1, unpaired_weight=0.1, leaf_weight=1.):
    if seed:
        random.seed(seed)
    if n <= 0:
        return ''
    else:
        r = random.random()*sscount(n, is_nested, count, theta=theta, unpaired_weight=unpaired_weight,leaf_weight=leaf_weight)
        r -= unpaired_weight*sscount(n-1, is_nested, count, theta=theta, unpaired_weight=unpaired_weight,leaf_weight=leaf_weight)
        if r < 0:
            return '.'+ssrandom(n-1, is_nested, count, theta=theta, unpaired_weight=unpaired_weight,leaf_weight=leaf_weight)
        for i in range(theta+2, n+1):
            r -= sscount(i-2, True, count, theta=theta, unpaired_weight=unpaired_weight,leaf_weight=leaf_weight)*sscount(n-i, False, count, theta=theta, unpaired_weight=unpaired_weight,leaf_weight=leaf_weight)
            if r < 0:
                return '('+str(ssrandom(i-2, True, count, theta=theta, unpaired_weight=unpaired_weight,leaf_weight=leaf_weight))+')'+str(ssrandom(n-i, False, count, theta=theta, unpaired_weight=unpaired_weight,leaf_weight=leaf_weight))
            

def compatiblepairs(p1, p2):
    (i, j) = p1
    (k, l) = p2
    return (i < j < k < l) or (i < k < l < j) or (k < l < i < j) or (k < i < j < l)


def printpath(p, B, R):
    bpsB = sstopairs(B)
    bpsR = sstopairs(R)
    tmp = [c for c in ssstring(B)]
    h = 0
    print("".join(tmp), "h=", h)
    for (i, j) in p:
        if (i, j) in bpsB:
            (tmp[i], tmp[j]) = ('.', '.')
            h += 1
        if (i, j) in bpsR:
            (tmp[i], tmp[j]) = ('[', ']')
            h -= 1
        print("".join(tmp), "h=", h)


def IInverse(Sep, B, R):
    # Function I is its own inverse
    return I(Sep, B, R)


def I(Sep, B, R):
    bpsB = sstopairs(B)
    bpsR = sstopairs(R)
    return (bpsB-Sep, bpsR & Sep)


def separate(B, R, theta=1):
    def MCF(B, R, theta=1):
        # Returns max #pairs, and matching structure in well-parenthesized
        # notation, composed of compatible pairs in B + R (=E)
        # restricted to interval [i,j], and further constrained to
        # featuring >0 pair for B (iff alpha=True) or R (iff beta=True)
        # mat is just a caching dictionary for memoization
        # Note: It takes strength and discipline to be *this* lazy...
        mat = {}
        n = len(B)
        # Init for intervals smaller than THETA nucleotides
        for m in range(1, theta+1):
            for alpha, beta in boolTuples(2):
                for i in range(0, n-m+1):
                    j = i+m-1
                    if (alpha, beta) == (False, False):
                        # If interval too small, and no need to include
                        # pairs from either B or R (alpha,beta == False, False)
                        # Then the best structure has 0 pairs
                        mat[(i, j, alpha, beta)] = (0, "."*m)
                    else:
                        # If interval too small, but we still need to include
                        # some pair from B or R (alpha,beta != False, False)
                        # Then there is no allowed structure (-infty pairs)
                        mat[(i, j, alpha, beta)] = (-sys.maxsize, "X"*m)
        for m in range(theta+1, n+1):
            for alpha, beta in boolTuples(2):
                for i in range(0, n-m+1):
                    j = i+m-1

                    # Case 1: First position unpaired, max #pairs is found by
                    # recurrence on [i+1,j], forwarding the need to feature pair
                    # from B (iff alpha=True) or R (iff beta=True)
                    (k, ss) = mat[(i+1, j, alpha, beta)]
                    mat[(i, j, alpha, beta)] = (k, "."+ss)

                    # Case 2: First position paired, max #pairs is found by
                    # recurrence on [i+1,j], forwarding the need to feature pair
                    # from B (iff alpha=True) or R (iff beta=True)

                    # Check if pairs from B and R at pos i can be considered
                    # for the paired case (ie must be fully contained in [i,j])
                    partners = []
                    if B[i] > i and B[i] <= j:
                        partners.append((i, B[i]))
                    if R[i] > i and R[i] <= j:
                        partners.append((i, R[i]))
                    # Go over suitable partners for pos. i (within R or B)
                    for (a, k) in partners:
                        assert(a == i)
                        # Consider all possible ways of "forwarding" constraints
                        # (having >0 pair from B, ie alpha=True, or R, ie beta=True),
                        # to subintervals [i+1,k-1] (alpha1,beta1) and [k+1,j] (alpha2,beta2).
                        for (alpha1, beta1, alpha2, beta2) in boolTuples(4):
                            # If alpha=True, check that "someone" satisfies constraint
                            # of having >0 pair from B (ie either through pair (a,k),
                            # recursion over [i+1,k-1], or rec. over [k+1,k])
                            if not alpha or (alpha1 or alpha2 or B[i] == k):
                                # Same for R w.r.t. beta1 and beta2
                                if not beta or (beta1 or beta2 or R[i] == k):
                                    # Recurse over subintervals with suitable constraints
                                    if i+1 <= k-1:
                                        (termA, ssA) = mat[(
                                            i+1, k-1, alpha1, beta1)]
                                    elif not alpha1 and not beta1:
                                        (termA, ssA) = (0, "")
                                    else:
                                        (termA, ssA) = (-sys.maxsize, "X")
                                    if k < j:
                                        (termB, ssB) = mat[(
                                            k+1, j, alpha2, beta2)]
                                    elif not alpha2 and not beta2:
                                        (termB, ssB) = (0, "")
                                    else:
                                        (termB, ssB) = (-sys.maxsize, "X")
                                    # Accumulate max #pairs, plus one for current pair (a,k)
                                    mat[(i, j, alpha, beta)] = max(
                                        mat[(i, j, alpha, beta)], (1+termA+termB, "("+ssA+")"+ssB))
        return mat[(0, n-1, True, True)]
    assert(len(B) == len(R))
    # Call to rec. function, considering full interval [i,j] = [0,n-1],
    # and forcing >0 pair from both B and R with (alpha,beta) = (True,True)
    (k, sep) = MCF(B, R, theta=theta)
    if DEBUG:
        print("            MCF", k, sep)

    # I constraints cannot be satisfied, return nothing
    if k < 0:
        return None
    # Convert well-parenthesized separator struct into arc-annotated seq...
    ss = ssparse(sep)
    # ... and then to set of base pairs
    bps = sstopairs(ss)
    # Check that the prediction max #pairs and effective #pairs match
    assert(k == len(bps))
    if k < max(len(sstopairs(B)), len(sstopairs(R))):
        return None
    else:
        return IInverse(bps, B, R)


def delta(B, R):
    return len(B)-len(R)


def builddependencies(B, R):
    res = []
    for p1 in R:
        res.append((p1, [p2 for p2 in B if not compatiblepairs(p1, p2)]))
    tmp = sorted([(len(deps), p, deps) for p, deps in res])
    # print tmp
    return [(p, set(deps)) for (m, p, deps) in tmp]


def skipInclusions(allDeps):
    # assume allDeps is sorted by deps len,
    # filter out (p1,d1) if it contains (p2,d2)
    # (can be assumed to not appear first)
    if LEAVEINCLUDES:
        return allDeps
    res = []
    for (p1, deps1) in allDeps:
        for (p2, deps2) in res:
            if deps2.issubset(deps1):
                break
        else:
            res.append((p1, deps1))
    return res


def skipBlacklist(bl, allDeps):
    if LEAVEBLACKLIST:
        return allDeps
    res = []
    for (p, deps) in allDeps:
        if p not in bl:
            res.append((p, deps))
    return res


def no_conflicts(bpsB, bpsR):
    for i, j in bpsB:
        for k, l in bpsR:
            if (i <= k and k <= j and j <= l) or (k <= i and i <= l and l <= j):
                return False
    return True

def list_deps(bpR, bpsB):
    i,j = bpR
    dependencies = []
    for k,l in bpsB:
        if (i <= k and k <= j and j <= l) or (k <= i and i <= l and l <= j):
            dependencies.append((k,l))
    return dependencies

def realize(B, R, k, depth=0, firstRblacklist=set([]), lastBblacklist=set([]), theta=1):
    def indent(depth):
        return "  "*depth
    global maxk
    if DEBUG:
        print(indent(depth)+"realize (k=%s):" % k)
        print(indent(depth+1)+"B=%s" % (ssstring(B)))
        print(indent(depth+1)+"R=%s" % (ssstring(R)))
    if k > maxk:
        maxk = k

    n = len(B)
    assert(n == len(R))
    if k < 0:
        return None

    bpsB = sstopairs(B)
    bpsR = sstopairs(R)

    if len(bpsB) == 0:
        if DEBUG:
            print(indent(depth+1)+"|B|=0   -> Return R")
        return list(bpsR)

    for bpR in bpsR:
        if len(list_deps(bpR, bpsB))==0:
            stock = realize(B, pairstoss(bpsR-set([bpR]), n), 
                                 k+1, 
                                 depth=depth+1, 
                                 firstRblacklist=firstRblacklist, 
                                 lastBblacklist=lastBblacklist, 
                                 theta=theta)
            if stock != None:
                return [bpR]+stock
            else:
                return None

#    if no_conflicts(bpsB, bpsR):
#        return list(bpsR)+list(bpsB)

    if k == 0:
        return None

    if len(bpsR) == 0:
        if DEBUG:
            print(indent(depth+1)+"|R|=0   -> Return B")
        if len(bpsB) > k:
            return None
        else:
            return list(bpsB)

    if len(bpsB) <= k:
        if DEBUG:
            print(indent(depth+1)+"|B|<=k   -> Return B.R")
        return list(bpsB) + list(bpsR)

    if len(bpsB) < len(bpsR):
        if DEBUG:
            print(indent(depth+1)+"|B|<|R| -> Swap B<->R")
        k += (len(bpsR)-len(bpsB))
        res = realize(R, B, k, depth, lastBblacklist,
                      firstRblacklist, theta=theta)
        if res is None:
            return None
        else:
            return list(reversed(res))

    # Check if separator can be found to divide and conquer
    sep = separate(B, R, theta=theta)
    if sep is not None:
        (bpsBp, bpsRp) = sep
        (bpsBi, bpsRi) = I(bpsBp | bpsRp, B, R)
        if DEBUG:
            print(indent(depth+1)+"Sep!=0  -> I=",
                  ssstring(pairstoss(bpsBi | bpsRi, n)))
        ssBLeft = pairstoss(bpsBp, n)
        ssRLeft = pairstoss(bpsRp, n)
        ssBRight = pairstoss(bpsB-bpsBp, n)
        ssRRight = pairstoss(bpsR-bpsRp, n)
        resL = realize(ssBLeft, ssRLeft, k, depth+1,
                       firstRblacklist, set([]), theta=theta)
        if resL is None:
            return None
        resR = realize(ssBRight, ssRRight, k-delta(bpsBp, bpsRp),
                       depth+1, set([]), lastBblacklist, theta=theta)
        if resL is not None and resR is not None:
            return resL + resR
        else:
            return None
    else:  # sep is None
        nextBlackList = firstRblacklist.copy()
        if DEBUG:
            print(indent(depth+1)+"Blacklists: first R= %s  last B = %s" %
                  (firstRblacklist, lastBblacklist))
        dependencies = builddependencies(bpsB, bpsR)
        if DEBUG:
            print(indent(depth+1)+" tries before filter %s" %
                  (len(dependencies)))
        dependencies = skipBlacklist(
            firstRblacklist, skipInclusions(dependencies))
        if DEBUG:
            print(indent(depth+1)+" tries after filter %s" %
                  (len(dependencies)))

        if DEBUG:
            print(indent(depth+1)+"Sep=0   -> Choosing first elem in R")
        for (p, deps) in dependencies:
            #assert(len(deps) > 1)  # otherwise |B|<=k or sep is not None
            (i, j) = p
            (bpsnB, bpsnR) = (bpsB-deps, bpsR - set([(i, j)]))
            if DEBUG:
                print(indent(depth+1) +
                      "Try (%s,%s) in R -> Remove %s from B" % (i, j, deps))
            if len(deps) <= k:
                #print(indent(depth+1)+"nextBL = %s " % nextBlackList)
                recres = realize(pairstoss(bpsnB, n), pairstoss(
                    bpsnR, n), k+1-len(deps), depth+2, nextBlackList, lastBblacklist, theta=theta)
                if recres is not None:
                    return list(deps) + [(i, j)] + recres
                nextBlackList.add(p)
    return None


def printRevPath(bpsC, p, n):
    ss2 = pairstoss(bpsC, n)
    print(bpsC, ssstring(ss2))
    for (i, j) in reversed(p):
        if (i, j) in bpsC:
            bpsC = bpsC - set([(i, j)])
        else:
            bpsC = bpsC | set([(i, j)])
    ss1 = pairstoss(bpsC, n)
    printpath(p, ss1, ss2)


MIN_OF_MAX = 1


def bruteforce(C, B, R, n, P=[], curr_h=0, curr_max_h=0, h={}):
    # print len(C),len(B),len(R),len(P),curr_h,curr_max_h

    if MIN_OF_MAX not in h:
        h[MIN_OF_MAX] = +sys.maxsize

# if DEBUG:
##        ssC = pairstoss(C,n)
##        ssB = pairstoss(B,n)
##        ssR = pairstoss(R,n)
# print ssstring(ssC),ssstring(ssB),ssstring(ssR),P,curr_h,curr_max_h,h

    if curr_max_h > h[MIN_OF_MAX]:
        return (sys.maxsize, [])

    if len(R) == len(B) == 0:
        if curr_max_h < h[MIN_OF_MAX]:
            ssR = pairstoss(R, n)
            ssC = pairstoss(C, n)
            x = h[MIN_OF_MAX]
            if h[MIN_OF_MAX] == sys.maxsize:
                x = "+infty"
            else:
                x = "%s" % (x)
            print("[Update min h: %s -> %s]" % (x, curr_max_h))
            h[MIN_OF_MAX] = min(h[MIN_OF_MAX], curr_max_h)
        return (curr_max_h, P)

    # Free elements from R should be added as early as possible (no combinatorial exploration)
    for p1 in R:
        canBeAdded = True
        for p2 in C:
            if not compatiblepairs(p1, p2):
                canBeAdded = False
        if canBeAdded:
            addset = set([p1])
            return bruteforce(C | addset, B, R-addset, n, P+[p1], curr_h-1, curr_max_h, h)

    # If no elements left in R, remove pairs from B in any order
    if len(R) == 0:
        ncurr_h = curr_h+len(B)
        return bruteforce(C-B, set([]), set([]), n, P+list(B), ncurr_h, max(curr_max_h, ncurr_h), h)

    # An element is left in R, but not free, investigate all possibilities
    bestPath = (sys.maxsize, [])
    for (p1, deps) in builddependencies(C, R):
        # Destroy |deps| pairs in B (+|deps|), create one pair in B
        ncurr_h = curr_h+len(deps)-1
        # Max height reached after destroyung |deps| pairs in B
        local_max_h = curr_h+len(deps)
        res = bruteforce(C-deps | set([p1]), B-deps, R-set([p1]), n,
                         P+list(deps)+[p1], ncurr_h, max(curr_max_h, local_max_h), h)
        (height, p) = res
        bestPath = min(bestPath, (height, p))
    return bestPath


if __name__ == "__main__":
    # Set random seed for sake of reproducibility
    seed = 0
    n = 50
    maxAllowedK = -1
    try:
        opts, args = getopt.getopt(sys.argv[1:], "n:s:idtfbk:")
    except getopt.GetoptError:
        print('-n <size> -s <seed> -i (leave includes) -d (debug) -t (run timer, silent mode) -f (brute force) -b (leave blacklists) -k <max allowed k>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-n':
            n = int(arg)
        elif opt == '-s':
            seed = int(arg)
        elif opt == '-k':
            maxAllowedK = int(arg)
        elif opt == '-i':
            LEAVEINCLUDES = True
        elif opt == '-b':
            LEAVEBLACKLIST = True
        elif opt == '-f':
            RUNBRUTEFORCE = True
        elif opt == '-d':
            DEBUG = True
        elif opt == '-t':
            TIME = True
    # print csv format: n, seed, options, k,maxk,<empty>,<empty>, time
    sys.stdout.write("%s,%s,v3b_%s," % (
        n, seed, ("skipIncludes" if not LEAVEINCLUDES else "base")+("_skipBL" if not LEAVEBLACKLIST else "")))
    if TIME:
        saveStdout = sys.stdout
        f = open(os.devnull, 'w')
        sys.stdout = f
        start_time = time.time()

    random.seed(seed)
    count = {}

    s1 = ssrandom(n, count)
    s2 = ssrandom(n, count)
    if s2.count("(") > s1.count("("):
        s1, s2 = s2, s1

    ss1 = ssparse(s1)
    ss2 = ssparse(s2)
    bps1 = sstopairs(ss1)
    bps2 = sstopairs(ss2)

    # Remove common base pairs
    commonbps = bps1 & bps2
    if len(commonbps) > 0:
        for (i, j) in commonbps:
            (ss1[i], ss1[j]) = (-1, -1)
            (ss2[i], ss2[j]) = (-1, -1)
        s1 = ssstring(ss1)
        s2 = ssstring(ss2)
        bps1 = sstopairs(ss1)
        bps2 = sstopairs(ss2)

    print(s1, len(sstopairs(ss1)))
    print(s2, len(sstopairs(ss2)))

    if maxAllowedK < 0:
        maxAllowedK = n
    print("[Running XP algo]")
    for k in range(0, maxAllowedK+1):
        print("  [k=%s]" % k)
        p = realize(ss1, ss2, k, 1)
        if p is not None:
            print(s1, "Origin")
            printpath(p, ss1, ss2)
            print(s2.replace("(", "[").replace(")", "]"), "Destination")
            break
        else:
            print("    No solution found")
            if k == maxAllowedK:
                k = k+0.5

    if TIME:
        sys.stdout = saveStdout
        print("%s,%s,,,%s" % (k, maxk, time.time() - start_time))

    if RUNBRUTEFORCE:
        print("[Running branch and bound algo]")
        (minh, p) = bruteforce(bps1, bps1, bps2, n)
        print(s1, "Origin")
        printpath(p, ss1, ss2)
        print(s2.replace("(", "[").replace(")", "]"), "Destination")
        # sys.exit()


# for i in {0..15}; do echo $i; python ./barriers.py -n 50 -t -s $i>>times;   python ./barriers.py -n 50 -ti -s $i >>times; done
