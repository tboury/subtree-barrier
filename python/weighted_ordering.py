import sys
sys.setrecursionlimit(10000)
from itertools import product
from cpp_routines import includer, detecter, computer_N_star, adder, difference, barrier, barrier_w, reducer_adjacency
import random
#from schedule_weighted_nodes_ranked import SUBT_build, build_tree_whole, full_rank #TODO: introduction of a ranking function

def generate_scheduling(B, R, Adjacency, predecessor, DP_matrix, weight, valid_subtrees):
    """
Input : H is the bipartite graph that deals with the scheduling constraints decomposed in the B and R and also an adjacency list Adjacency.
B : negative constraints/nodes/arcs
R : positive constraints/nodes/arcs
predecessor : the list that gives for each arc of index i the above blue arc in the spanning tree
DP_matrix : DP matrix that stored the precomputed orderings and barriers for instances of smaller sizes.

Output : ordering : An optimal ordering  for H
K : The optimal barrier
    """
    ordering = []
    if len(B) == 0 and len(R) == 0:
        return (ordering, 0)
    if weight:
        B_weight = measure_weight(B, weight)
    else:
        B_weight = len(B)
    if len(R) == 0: #try to catch the boundary case where only blues remain
        ordering += B
        return (ordering, B_weight)

    for K in range(B_weight + 1): #test all the possible barriers
        for b in B: #test possibilities for last blues
            ST1, ST2 = split(B, b, predecessor)
            try_merge = first_merge(ST1,  ST2,  K, Adjacency, DP_matrix, weight, valid_subtrees) #merge will take subtrees and orderings in entries

            if try_merge[1] != -1 and barrier_weight(ordering + try_merge[0] + [b] + Adjacency[b], B, weight) > K:
                try_merge = first_merge(ST1,  ST2,  K - 1, Adjacency, DP_matrix, weight, valid_subtrees)
            if try_merge[1] != -1 and barrier_weight(ordering + try_merge[0] + [b] + Adjacency[b], B, weight) <= K:  
                ordering += try_merge[0]
                ordering += [b] + Adjacency[b]
                return (ordering,  barrier_weight(ordering, B, weight))

def compute_N_star(list_blues, Adjacency):
    """ Input a list of blues list_blues which returns N_star depending of the constraints conditions known from H_whole_Adjacency"""
    #list_blues.sort() #not useful here as the list was already sorted before in any case
    return computer_N_star(list_blues, Adjacency)

def split(s, b, predecessor):
    """
    split a group of blue arc s in two subtrees according to blue b
    """
    children= [i for i in range(len(predecessor))
              if predecessor[i] == b]
    subtree1 = []
    while children!= []:
        elem = children.pop()
        if elem not in subtree1 and elem in s: #TODO : check here
            children+= [i for i in range(len(predecessor))
                       if predecessor[i] == elem]
            subtree1.append(elem)
    subtree2 = [i for i in s if i not in subtree1 and i != b]
    subtree1.sort()
    subtree2.sort()
    return (subtree1, subtree2)


def init_tree_circle_formalism(RNA_instance, debug=0):
    """
    A function that build the tree in circle formalism and predecessor list that serve in all operation as it stored the edges of the trees
    Input : RNA_instance : THE RNA instance in term of well parenthesed word that corresponds to studied instance.
    Output : predecessor : The list that indicates the arc that is the parent of each other blue arc. 
    """
    numero = 0
    predecessor = [-1]
    next_index = 1
    for i in range(len(RNA_instance)):
        if RNA_instance[i] == '(':
            predecessor.append(numero)
            numero = next_index
            next_index += 1
        if RNA_instance[i] == ')':
            numero = predecessor[numero]
    if debug:
        print("predecessor",predecessor)
    return predecessor


def solve(Whole_B, Whole_R, Adjacency, RNA_instance, debug, totest, predecessor, weight):
    """Input : H is the bipartite graph that deals with the scheduling constraints devided in Whole_B and Whole_R. Adjacency is the list of adjacency for graph H.
Whole_B : whole negative constraints/nodes/arcs
Whole_R : whole positive constraints/nodes/arcs
RNA_instance : The RNA instance in term of well parenthesed words that corresponds to graph H
Output : ordering : An optimal ordering  for H
K : The optimal barrier"""

    valid_subtrees = enumerate_subtrees(predecessor)
    valid_subtrees.sort(key = len)
    list_rank = []
    #MAX_RANK = full_rank(predecessor,Whole_B, Whole_T, SUBT) #RANK(Whole_T, 0 , Whole_T, SUBT) # TODO: Change with a ranking
    DP_matrix =[([], -1) for i in range(len(valid_subtrees))]
    for (i,B) in enumerate(valid_subtrees): #already sort by size in valid_subtrees
        N_star = compute_N_star(B, Adjacency)
        reduced_adjacency = reduce_adjacency(Adjacency, B, N_star)
        new = generate_scheduling(B, N_star , reduced_adjacency, predecessor, DP_matrix, weight, valid_subtrees)
        #T = build_tree(predecessor, B) # TODO: Change with a ranking
        #print(new)
        #if full_rank(predecessor, B, Whole_T, SUBT) in list_rank:
        #r =full_rank(predecessor, B, Whole_T, SUBT)
        #list_rank.append((r,B))
        DP_matrix[i] = new
    (ordering, barrier) = DP_matrix[-1]
    advantage = 0
    top = []
    for r in Whole_R:
        if r not in ordering:
            top.append(r)
            if weight:
                advantage+=weight[r]
            else:
                advantage+=1
    if debug:
        print("\n(Ordering, Barrier) :")
    if totest:
        return (top+ordering, max(barrier - advantage, 0), predecessor, Whole_B)
    else:
        return (top+ordering, max(barrier - advantage, 0))


def enumerate_subtrees_from_root(list_edges, root):
    """The recursive process to count subtrees rooted in node root, knowing the edges that were not cut for now"""
    list_of_cut = [list_edges[i] for i in range(len(list_edges)) if (list_edges[i][0] == root or list_edges[i][1] == root) ]
    if list_of_cut == []:
        return [{root}]
    list_edges = list(set(list_edges) - set(list_of_cut))
    subtrees = [[[]] + enumerate_subtrees_from_root(list_edges, i) for (i,root) in (list_of_cut)]
    resu = []
    for new_tree in product(*subtrees):
        tree = []
        for sub in new_tree:
            tree+=sub
        tree = tree + [root]
        tree.sort()
        resu.append(set(tree))
    return resu


def enumerate_subtrees(predecessor):
    """Build, knowing the parentality of each blue arc in list of predecessors, the instances of all subtrees that could be extracted from the blue spanning tree"""
    # FOR THE MOMENT in K(\phi) n^{\phi + 2}
    # cost is elsewhere higher so it is not really a problem here could be refined with memoization.
    list_edges = []
    for blue in range(1, len(predecessor)):
        list_edges.append((blue, predecessor[blue]))
        # We can remark here that the first node of the edge is also the arc from which it "depends"
    subtrees = []
    for root in range(len(predecessor)):
        for counted in enumerate_subtrees_from_root(list_edges, root):
            subtrees.append( tuple(counted))
        subtrees= list(set(subtrees))
    # We have now the nodes for each subtrees, we want now only the arcs involved in each subtrees
    resu = []
    #resu_node = []
    one = 0
    for tree in subtrees:  # O(K(\phi))
        resu_tree = []
        for (i, j) in list_edges:  # O(n)
            if i in tree and j in tree: # O(n)
                resu_tree.append(i)
                #resu_node_tree[j].append(i)
        if resu_tree != [] or one == 0:
            if resu_tree == []:
                one = 1
            resu.append(resu_tree)
            #resu_node.append(resu_node_tree)
    return resu#, resu_node

def ind(valid_subtrees, ST):
    #TODO : could perhaps be refined with C++ procedure or a ranking ?
    return valid_subtrees.index(ST)


def first_merge(ST1, ST2, K, Adjacency, DP_matrix, weight,valid_subtrees):
    
    sigma1 = DP_matrix[ind(valid_subtrees, ST1)][0]
    sigma2 = DP_matrix[ind(valid_subtrees, ST2)][0]
    ordering = []

    """
    REMOVE SAFE UNITARY SETS
    """   
    #Pi is the blue subtree that is a SUS
    ST_before = adder(ST1, ST2)
    red_remaining = compute_N_star(ST_before, Adjacency)
    (P1, red_gain_P1, error1) = detect_SUS(ST1, sigma1, Adjacency, K, DP_matrix, weight)
    (P2,  red_gain_P2, error2) = detect_SUS(ST2, sigma2, Adjacency, K, DP_matrix, weight)
    while (error1 != -1 or error2 != -1):
        if error1 != -1:
            ordering+= red_gain_P1
            N_star = compute_N_star(P1, Adjacency)
            ordering += [r for r in N_star if r not in ordering]
            if weight:
                weight_P1,weight_N_star = measure_weight(P1, weight), measure_weight(N_star, weight)
            else:
                weight_P1, weight_N_star = len(P1), len(N_star)
            K = K  - weight_P1 + weight_N_star
            sigma1 = [i for i in sigma1 if i not in adder(P1, N_star)]
            ST1 =    [i for i in ST1 if i not in P1] #set(P1)
            remaining_blue = adder(ST1, ST2)
            remaining_red = [i for i in red_remaining if i not in ordering]
            Adjacency = reduce_adjacency(Adjacency, remaining_blue, remaining_red)          
        if error2 != -1:
            ordering+= red_gain_P2
            N_star = compute_N_star(P2, Adjacency)
            ordering += [r for r in N_star if r not in ordering]
            if weight:
                weight_P2,weight_N_star = measure_weight(P2, weight), measure_weight(N_star, weight)
            else:
                weight_P2, weight_N_star = len(P2), len(N_star)
            K = K  - weight_P2 + weight_N_star
            sigma2 = [i for i in sigma2 if i not in adder(P2, N_star)]
            ST2 =    [i for i in ST2 if i not in P2] #set(P2)
            remaining_blue = adder(ST1, ST2)
            remaining_red = [i for i in red_remaining if i not in ordering]
            Adjacency = reduce_adjacency(Adjacency, remaining_blue, remaining_red)      
        (P1, red_gain_P1,  error1) = detect_SUS(ST1, sigma1, Adjacency, K, DP_matrix, weight)
        (P2, red_gain_P2,  error2) = detect_SUS(ST2, sigma2, Adjacency, K, DP_matrix, weight)
    
    #merge will take subtrees and orderings in entries
    try_merge = merge(ST1, sigma1, ST2, sigma2, K, Adjacency, DP_matrix, weight) 
    
    
    if try_merge[1] != -1:
        return (ordering+try_merge[0], 1)
    else:
        return ([], -1)
   


def merge(ST1, sigma1, ST2, sigma2, K, Adjacency, DP_matrix, weight):
    
    ordering = []    
    
    """
    BASIC CASES FOR THE MERGE
    """   
    if ((sigma1 == []) or (sigma2 == [])):
        if ((barrier_weight(sigma1, ST1, weight) > K) or (barrier_weight(sigma2, ST2, weight) > K)):
            return ([], -1)
        else:
            return (ordering + sigma1 + sigma2, 1)


    """
    CORE OF THE ALGORITHM : TAKE Head SET AND NEXT SUS SET TO BUILD cl
    """
    Head1 = Head_set(ST1, sigma1, Adjacency)
    Head2 = Head_set(ST2, sigma2, Adjacency)
    
    # Build artificial sets and orderings based on how the real
    # orderings would be by removing Head and SUSs
    
    Star1 = compute_N_star(Head1, Adjacency)
    Whole_Star1 = Star1
    if weight:
        Whole_len1 = measure_weight(Head1, weight)
    else:
        Whole_len1 = len(Head1)
    Star2 = compute_N_star(Head2, Adjacency)
    Whole_Star2 = Star2
    if weight:
        Whole_len2 = measure_weight(Head2, weight)
    else:
        Whole_len2 = len(Head2)    
    Virtual_sigma1 = [i for i in sigma1 if i not in Head1 and i not in Star1]
    Virtual_ST1 = difference(ST1, Head1)
    Virtual_red_remainings1 = [r for r in sigma1 if r not in (ST1 + Star1)]
    Virtual_sigma2 = [i for i in sigma2 if i not in Head2 and i not in Star2]
    Virtual_ST2 = difference(ST2, Head2)
    if weight:
        Virtual_K1 = K - measure_weight(Head1, weight) + measure_weight(Star1, weight)
        Virtual_K2 = K - measure_weight(Head2, weight) + measure_weight(Star2, weight)
    else:
        Virtual_K1 = K - len(Head1) + len(Star1)
        Virtual_K2 = K - len(Head2) + len(Star2)
    Virtual_red_remainings2 = [r for r in sigma2 if r not in (ST2 + Star2)]
    Virtual_Adjacency1 = reduce_adjacency(Adjacency, Virtual_ST1, Virtual_red_remainings1)
    Virtual_Adjacency2 = reduce_adjacency(Adjacency, Virtual_ST2, Virtual_red_remainings2)
    
    """
    NOW TAKE SUCCESSIVES SAFE UNITARY SETS
    """
    (P1, red_gain_P1, error1) = detect_SUS(Virtual_ST1, Virtual_sigma1, Virtual_Adjacency1, Virtual_K1, DP_matrix, weight)
    (P2, red_gain_P2, error2) = detect_SUS(Virtual_ST2, Virtual_sigma2, Virtual_Adjacency2, Virtual_K2, DP_matrix, weight)
    Tail1 = []
    Tail2 = []
    while error1 != -1:
        Tail1.append((P1, red_gain_P1))
        Whole_len1 += len(P1)
        Star1 = compute_N_star(P1, Virtual_Adjacency1)
        Whole_Star1 += Star1
        Virtual_sigma1 = [i for i in Virtual_sigma1 if i not in P1 and i not in Star1]
        Virtual_ST1  =    [i for i in Virtual_ST1  if i not in P1] 
        if weight:
            Virtual_K1 = K - measure_weight(P1, weight) + measure_weight(Star1, weight)
        else:
            Virtual_K1 = K - len(P1) + len(Star1)
        Virtual_red_remainings1 = difference(Virtual_red_remainings1,Star1)
        Virtual_Adjacency1 = reduce_adjacency(Virtual_Adjacency1, Virtual_ST1, Virtual_red_remainings1)
        (P1, red_gain_P1, error1) = detect_SUS(Virtual_ST1,Virtual_sigma1, Virtual_Adjacency1, Virtual_K1, DP_matrix, weight)
    while error2 != -1:
        Tail2.append((P2, red_gain_P2))
        Whole_len2 += len(P2)
        Star2 = compute_N_star(P2, Virtual_Adjacency2)
        Whole_Star2 += Star2
        Virtual_sigma2 = [i for i in Virtual_sigma2 if i not in P2 and i not in Star2]
        Virtual_ST2 =    [i for i in Virtual_ST2 if i not in P2] 
        if weight:
            Virtual_K2 = K - measure_weight(P2, weight) + measure_weight(Star2, weight)
        else:
            Virtual_K2 = K - len(P2) + len(Star2)
        Virtual_red_remainings2 = difference(Virtual_red_remainings2,Star2)
        Virtual_Adjacency2 = reduce_adjacency(Virtual_Adjacency2, Virtual_ST2, Virtual_red_remainings2)
        (P2, red_gain_P2, error2) = detect_SUS(Virtual_ST2, Virtual_sigma2, Virtual_Adjacency2, Virtual_K2, DP_matrix, weight)

    """
    THE "cl" are NOW BUILT, WE WILL MAKE EVOLVE ORDERING DEPENDING ON WHICH FROM Tail1 or Tail2 WE SELECT
    """
    
    boolo = 0
    if weight == []:
        boolo = 1
    if measure_weight(Head1, weight, boolo) > K or barrier_weight(sigma2, ST2, weight) > K - Whole_len1 + measure_weight(Whole_Star1, weight, boolo):
        Tail1, Tail2 = Tail2, Tail1
        Head1, Head2 = Head2, Head1
        ST1, ST2 = ST2, ST1
        sigma1, sigma2 = sigma2, sigma1
        Virtual_K1 = Virtual_K2   


    """
    MAKE EVOLVE THE ORDERING CONCRETELY
    """
    if Tail1 == [] and Head1 == []:
        return ([], -1)
    if K - len(Head1) < 0: #If the use of a Head set breaks locally the budget (no more a problem when it comes to Safe Sets)
        return [], -1
    blues = Head1
    ordering += Head1 + compute_N_star(Head1, Adjacency)
 
    for index in range(len(Tail1)):
        (P, red_gain_P) = Tail1[index]
        ordering += red_gain_P
        blues += P
    blues.sort()
    N_star = compute_N_star(blues, Adjacency)
    ordering +=[r for r in N_star if r not in ordering]
    ordering = push_at_front(blues, ordering, Adjacency)
    #remove what was taken from the blue tree/ the ordering
    updated = [i for i in ST1 if i not in ordering]
    red_remaining = compute_N_star(adder(ST1, ST2), Adjacency)
    red_remaining = [i for i in red_remaining if i not in ordering]
    New_Adj = reduce_adjacency(Adjacency, updated, red_remaining)
    sigma1_updated = [i for i in sigma1 if i not in ordering]
    #sigma1_updated allows us to work with something that is not perhaps a subtree but that we know 
    #as an origin subtree from which we have removed a certain amount of Head sets / positive minimal sets
    if weight:
        Kprim = K + measure_weight(blues, weight) - measure_weight(N_star, weight)
    else:
        Kprim = K + len(blues) - len(N_star) #Warning : something still strange with Virtual_K1/ Virtual_K2... perhaps needs a fix ?
    K_small = 0
    stock = [], -1
    stock = merge(updated, sigma1_updated, ST2, sigma2, Kprim , New_Adj, DP_matrix, weight)
    if stock[1] != -1:
        ordering+= stock[0]
        return ordering, 1
    else:

        return [], -1


def Head_set(ST, sigma, Adjacency): 
    """ Compute the first Head set that could be extract from sigma knowing the color of task thanks to the blue subtree ST and knowing Adjacency of the graph
    """
    sigma = push_at_front(ST,sigma, Adjacency) #We put all the red that we can in a "as soon as possible pattern"
    red = sigma[0] #The first one would be the candidate that would be necessarely in the Head set
    i = 1
    while red in ST and i < len(sigma):
        red = sigma[i]
        i = i + 1
    if sigma[i - 1] in ST:
        return sigma #only blues remains
    return [b for b in ST if red in Adjacency[b]]

def push_at_front(ST,sigma, Adjacency):
    reds = [red for red in sigma if red not in ST] #identify the reds
    for red in reds:
        i = sigma.index(red) #retrieve current index of the red
        left = sigma[:i]
        right = sigma[(i + 1):]
        while give_bool(ST,sigma, Adjacency, left, i, red):
            i = i-1
        sigma = left[:i] + [red] + left[i:] + right
    return sigma

def give_bool(ST,sigma, Adjacency, left, i, red):
    if i == 0:
        return False
    if left[i - 1] in ST:
        if red in Adjacency[left[i - 1]]:
            return False
    return True

def reduce_adjacency(adjacency, tree, N_star):
    return reducer_adjacency(adjacency, tree, N_star)



def barrier_weight(ordering, B, weight):
    if weight:
        return barrier_w(ordering, B, weight)
    else:
        return barrier(ordering, B)


def detect_SUS(B, sigma, Adjacency, K, DP_matrix, weight):
    #Now computed in C++ with a loop on all indexes that could be mapped with subtrees
    return detecter(B, sigma, Adjacency, K, DP_matrix, weight)

def include(li1, li2):
    #Now computed in C++
    return includer(li1, li2)

    
def ssparse(seq):
    res = [-1 for c in seq]
    p = []
    for i, c in enumerate(seq):
        if c == '(':
            p.append(i)
        elif c == ')':
            j = p.pop()
            (res[i], res[j]) = (j, i)
    return res


def sstopairs(ss):
    res = []
    for i in range(len(ss)):
        if ss[i] > i:
            j = ss[i]
            res.append((i, j))
    return res


def seq_to_adjacency(blue, red):
    blue_pairs = sstopairs(blue)
    red_pairs = sstopairs(red)
    resu = [[] for k in range(len(blue_pairs) + 1 + len(red_pairs))]
    for rindex,(red1,red2) in enumerate(red_pairs):
        for bindex,(blue1, blue2) in enumerate(blue_pairs):
            bool = 0
            if red1 in range(blue1, blue2 + 1):
                if red2 not in range(blue1 + 1, blue2 ):
                    bool = 1 
            if red2 in range(blue1, blue2 + 1):
                if red1 not in range(blue1 + 1, blue2 ) :
                    bool = 1  
            if bool == 1:
                resu[bindex + 1].append(rindex + 1 + len(blue_pairs)) 
    resu[0] = [len(blue_pairs) , len(red_pairs)]
    for blue in range(1, resu[0][0] + 1):
        for red in resu[blue]:
            resu[red].append(blue) 
    return resu

def main_subtree(blue_seq, red_seq, debug = 0, totest = 0, pred = [], weight =[]): #, weighted = 0): #, reverse = 0, decision = 0):

    #OLD CODE FOR WHEN YOU WANT TO USE A SIMPLE HEURISTIC TO CHOOSE IF WE WANT TO SOLVE BLUES TO REDS OR REDS TO BLUES
    #if decision:
    #    predecessor_blue = init_tree_circle_formalism(blue_seq, debug) 
    #    if reverse == 0:
    #        predecessor_red = init_tree_circle_formalism(red_seq, debug)
    #        choice = 0
    #        leafs_blues = count_leafs(predecessor_blue)
    #        #print("blue_leafs", leafs_blues)
    #        leafs_reds = count_leafs(predecessor_red)
    #        #print("red_leafs", leafs_reds)
    #        #print(" ",theoretical_complexity( len(predecessor_blue),leafs_blues), "\n", theoretical_complexity(len(predecessor_red),leafs_reds))       
    #        #if theoretical_complexity(len(predecessor_blue), leafs_blues) >theoretical_complexity(len(predecessor_red), leafs_reds):
    #        #    choice = 1
    #        if leafs_reds < leafs_blues:
    #            choice = 1
    #        if leafs_reds == leafs_blues:
    #            if len(predecessor_blue) > len(predecessor_red):
    #                choice = 1
    #        if choice:    
    #            return main_subtree(red_seq, blue_seq, debug, totest, 1, predecessor_red, decision=0)
    #    pred = predecessor_blue
    #else:
    pred = init_tree_circle_formalism(blue_seq, debug) 
    Whole_B = [(i + 1) for i,c in enumerate(sstopairs(ssparse(blue_seq)))]
    Whole_R = [(i + 1 + len(Whole_B)) for i,c in enumerate(sstopairs(ssparse(red_seq)))]
    Adjacency =  seq_to_adjacency(ssparse(blue_seq), ssparse(red_seq))
    if debug:
        if weight: 
            print("Blues : ",Whole_B, "\nReds : ", Whole_R, "\nAdjacency :", Adjacency, "\nWeight :", weight)
        else:
            print("Blues : ",Whole_B, "\nReds : ", Whole_R, "\nAdjacency :", Adjacency)
    if totest:
        (elem1, elem2, elem3, elem4) = solve(Whole_B, Whole_R, Adjacency, blue_seq, debug, totest, pred, weight)
        return (elem1, elem2, elem3, elem4)#, reverse)
    (elem1, elem2) = solve(Whole_B, Whole_R, Adjacency, blue_seq, debug, totest, pred, weight)
    if debug:
        print(elem1, elem2)#, reverse)
    return (elem1, elem2)#, reverse)


def count_leafs(predecessor):
    leafs = [i for i in range(1,len(predecessor))]
    count0 = len([parent for parent in predecessor if parent == 0])
    leafs = list(set(leafs) - set(predecessor))
    nb_leafs = len(leafs)
    if count0 == 1:
        nb_leafs+=1
    return nb_leafs

def give_weight_to_nodes(Whole_B, Whole_R,seed):
    random.seed(seed)
    resu = [-1]
    for i in range(len(Whole_B) + len(Whole_R)):
        resu.append(random.randint(1,3))
    return resu

def measure_weight(S, weight, boolo = 0):
    if boolo == 1:
        return len(S)
    resweight = 0
    for s in S:
        resweight += weight[s]
    return resweight

#def theoretical_complexity(n, phi):
#    return  n**phi#/(phi**phi) #(2**(phi -1) + phi - 1)


