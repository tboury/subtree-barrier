import csv
from barriers_py3 import boolTuples, ssrandom, ssparse, sstopairs, ssstring
import sys
import random

def MCFbis(B, R, len_wholeB, k, theta=1):
    #Only here to serve for range computation
    def MCF(B, R, theta=1):
        # Returns max #pairs, and matching structure in well-parenthesized
        # notation, composed of compatible pairs in B + R (=E)
        # restricted to interval [i,j], and further constrained to
        # featuring >0 pair for B (iff alpha=True) or R (iff beta=True)
        # mat is just a caching dictionary for memoization
        # Note: It takes strength and discipline to be *this* lazy...
        mat = {}
        n = len(B)
        # Init for intervals smaller than THETA nucleotides
        for m in range(1, theta+1):
            for alpha, beta in boolTuples(2):
                for i in range(0, n-m+1):
                    j = i+m-1
                    if (alpha, beta) == (False, False):
                        # If interval too small, and no need to include
                        # pairs from either B or R (alpha,beta == False, False)
                        # Then the best structure has 0 pairs
                        mat[(i, j, alpha, beta)] = (0, "."*m)
                    else:
                        # If interval too small, but we still need to include
                        # some pair from B or R (alpha,beta != False, False)
                        # Then there is no allowed structure (-infty pairs)
                        mat[(i, j, alpha, beta)] = (-sys.maxsize, "X"*m)
        for m in range(theta+1, n+1):
            for alpha, beta in boolTuples(2):
                for i in range(0, n-m+1):
                    j = i+m-1

                    # Case 1: First position unpaired, max #pairs is found by
                    # recurrence on [i+1,j], forwarding the need to feature pair
                    # from B (iff alpha=True) or R (iff beta=True)
                    (k, ss) = mat[(i+1, j, alpha, beta)]
                    mat[(i, j, alpha, beta)] = (k, "."+ss)

                    # Case 2: First position paired, max #pairs is found by
                    # recurrence on [i+1,j], forwarding the need to feature pair
                    # from B (iff alpha=True) or R (iff beta=True)

                    # Check if pairs from B and R at pos i can be considered
                    # for the paired case (ie must be fully contained in [i,j])
                    partners = []
                    if B[i] > i and B[i] <= j:
                        partners.append((i, B[i]))
                    if R[i] > i and R[i] <= j:
                        partners.append((i, R[i]))
                    # Go over suitable partners for pos. i (within R or B)
                    for (a, k) in partners:
                        assert(a == i)
                        # Consider all possible ways of "forwarding" constraints
                        # (having >0 pair from B, ie alpha=True, or R, ie beta=True),
                        # to subintervals [i+1,k-1] (alpha1,beta1) and [k+1,j] (alpha2,beta2).
                        for (alpha1, beta1, alpha2, beta2) in boolTuples(4):
                            # If alpha=True, check that "someone" satisfies constraint
                            # of having >0 pair from B (ie either through pair (a,k),
                            # recursion over [i+1,k-1], or rec. over [k+1,k])
                            if not alpha or (alpha1 or alpha2 or B[i] == k):
                                # Same for R w.r.t. beta1 and beta2
                                if not beta or (beta1 or beta2 or R[i] == k):
                                    # Recurse over subintervals with suitable constraints
                                    if i+1 <= k-1:
                                        (termA, ssA) = mat[(
                                            i+1, k-1, alpha1, beta1)]
                                    elif not alpha1 and not beta1:
                                        (termA, ssA) = (0, "")
                                    else:
                                        (termA, ssA) = (-sys.maxsize, "X")
                                    if k < j:
                                        (termB, ssB) = mat[(
                                            k+1, j, alpha2, beta2)]
                                    elif not alpha2 and not beta2:
                                        (termB, ssB) = (0, "")
                                    else:
                                        (termB, ssB) = (-sys.maxsize, "X")
                                    # Accumulate max #pairs, plus one for current pair (a,k)
                                    mat[(i, j, alpha, beta)] = max(
                                        mat[(i, j, alpha, beta)], (1+termA+termB, "("+ssA+")"+ssB))
        return mat[(0, n-1, True, True)]
    #print(k, len(wholeB),  MCF(B, R, theta=1)[0])
    return max(0,k + MCF(B, R, theta=1)[0] - len_wholeB)




def build_local_stats(ss1, ss2, k3, theta, predecessor, timerr, n, partial_time, partial_time_full, counted_leafs):
    #Stats that are preprocessed by returning th mean of time for each graphs
    wholeB = [i for i  in ss1 if i != -1]
    rangee= MCFbis(ss1, ss2, len(wholeB)//2, k3, theta=theta)
    leafs = [i for i in range(1,len(predecessor))]
    count0 = 0
    for parent in predecessor:
        if parent == 0:
            count0 += 1
        if parent in leafs:
            leafs.remove(parent)
    nb_leafs = len(leafs)
    if count0 == 1:
        nb_leafs+=1
    bps1 = len([i for i in ss1 if i != -1])//2
    bps2 = len([i for i in ss2 if i != -1])//2
    print("leafs1, leafs2, range, bps1, bps2", nb_leafs,counted_leafs, rangee, bps1, bps2)
    for i in range(3):
        if (n, rangee,nb_leafs, bps1, bps2, counted_leafs) in partial_time[i]:
            (value, nb_runs) = partial_time[i][(n, rangee,nb_leafs, bps1, bps2, counted_leafs)]
            partial_time[i][(n, rangee,nb_leafs, bps1, bps2, counted_leafs)] = (value+ timerr[i], nb_runs + 1)
            (value) = partial_time_full[i][(n, rangee,nb_leafs, bps1, bps2, counted_leafs)]
            partial_time_full[i][(n, rangee,nb_leafs, bps1, bps2, counted_leafs)] = (value +[timerr[i]])
        else:
            partial_time[i][(n, rangee,nb_leafs, bps1, bps2, counted_leafs)] = (timerr[i], 1)
            partial_time_full[i][(n, rangee,nb_leafs, bps1, bps2, counted_leafs)] = ([timerr[i]])
    return (partial_time, partial_time_full)


def build_total_stats(partial_time, partial_time_full):
    #Whole stats graph depending of length of sequences n, the range and the number of leafs
    total_time = [[("n","range","nb_leafs1", "bps1", "bps2", "nb_leafs2")], ["Directed_Pathwidth_method"],["Maximum_Conflict_Free_method"],["Subtree method"]]
    total_time_full = [[("n","range","nb_leafs1", "bps1", "bps2", "nb_leafs2")], ["Directed_Pathwidth_method"],["Maximum_Conflict_Free_method"],["Subtree method"]]    
    sorting = []
    for blub,cle in enumerate(partial_time[0]):
        sorting.append(cle)
        sorting.sort()
    for cle in sorting:
        total_time[0].append(cle)
        value = partial_time_full[0][cle]
        for r in range(len(value)):
            total_time_full[0].append(cle)
        for i in range(3):
            (value, nb_runs) = partial_time[i][cle]
            total_time[i + 1].append(value/nb_runs)
            (value) = partial_time_full[i][cle]
            total_time_full[i + 1]+= value
    total_time_bis = [([total_time[0][j][0], total_time[0][j][1], total_time[0][j][2], total_time[0][j][5], total_time[0][j][3], total_time[0][j][4]]+[total_time[i][j] for i in range(1,len(total_time))]) for j in range(len(total_time[0]))]
    with open('performancies.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(total_time_bis)
    total_time_bis_full = [([total_time_full[0][j][0], total_time_full[0][j][1], total_time_full[0][j][2], total_time_full[0][j][5], total_time_full[0][j][3], total_time_full[0][j][4]]+[total_time_full[i][j] for i in range(1,len(total_time_full))]) for j in range(len(total_time_full[0]))]
    with open('performanciesfull.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerows(total_time_bis_full)

def initialize_sequences(n, seed, theta, uw, lw, rev, additional, count_fixed = 0):
    random.seed(seed)
    count = {}
    s1 = ssrandom(n, False , count, theta=theta, unpaired_weight=uw, leaf_weight = lw)
    if additional != []:
        #It means that we ask for different parameters for the constructions of s1 and s2
        if count_fixed:
            count = {}
        s2 = ssrandom(n, False ,count, theta=additional[1], unpaired_weight=additional[0], leaf_weight = additional[2])
    else:
        if count_fixed:
            count = {}
        s2 = ssrandom(n, False ,count, theta=theta, unpaired_weight=uw, leaf_weight = lw)

    (s1,s2,ss1,ss2,bps1,bps2) = rearrange_sequences(s1, s2, rev)
    return (s1,s2,ss1,ss2,bps1,bps2)

def rearrange_sequences(s1, s2, rev):

    #if s2.count("(") > s1.count("("):
    #    s1, s2 = s2, s1

    if rev == 1:
        s1, s2 = s2, s1

    ss1 = ssparse(s1)
    ss2 = ssparse(s2)
    bps1 = sstopairs(ss1)
    bps2 = sstopairs(ss2)

    # Remove common base pairs
    commonbps = bps1 & bps2
    if len(commonbps) > 0:
        for (i, j) in commonbps:
            (ss1[i], ss1[j]) = (-1, -1)
            (ss2[i], ss2[j]) = (-1, -1)
        s1 = ssstring(ss1)
        s2 = ssstring(ss2)
        bps1 = sstopairs(ss1)
        bps2 = sstopairs(ss2)
    return (s1,s2,ss1,ss2,bps1,bps2)
