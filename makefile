SO_LIB_NAME = python/cpp_routines`python3-config --extension-suffix`
FLAGS = -O3 -Wall -shared -std=c++11 -fPIC `python3 -m pybind11 --includes`
OSX_FLAGS = -O3 -Wall -shared -std=c++11 -undefined dynamic_lookup -fPIC `python3 -m pybind11 --includes`
SRC_FILE = cpp/cpp_routines.cpp

linux:
	c++ $(FLAGS) $(SRC_FILE) -o $(SO_LIB_NAME)

macos:
	c++ $(OSX_FLAGS) $(SRC_FILE) -o $(SO_LIB_NAME)

check:
	python3 -m pytest -s test/
	
checksilent:
	python3 -m pytest test/

fullcheck:
	coverage run --source=. -m pytest test/ 
	coverage report -m

checkprofile:
	python3 -m cProfile -s cumtime test/test_agreement_dpw_mcf.py |grep weighted_ordering

clean:
	rm -f python/cpp_routines.cpython-38-x86_64-linux-gnu.so
	rm -f performancies.csv
	rm -f performanciesfull.csv
	rm -fR __pycache__
	rm -fR python/__pycache__
	rm -fR test/__pycache__
