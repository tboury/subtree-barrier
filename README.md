
# Implementation of Subtree (Divide and Conquer) method



# Contents

This project contains a Makefile, one .cpp files for C++ routines that serves for methods and multiples python files, the core of the algorithm for the Subtree method are inside the file weighted_schedule.py.

* This project is heavily based on the folder for the implemention of Directed Pathwidth and Maximum Conflict Free methods from Laurent BULTEAU, Bertrand MARCHAND and Yann PONTY done in order to have some benchmarks for the following paper https://hal.inria.fr/hal-03272963v1.

In particular, it contains an implementation of https://link.springer.com/chapter/10.1007/978-3-642-25870-1_30,
an **XP algorithm for directed pathwidth** ($`O(n^{k+2})`$-time, $`O(n^{k+1})`$-space),
with the slight modification presented in the Appendix of https://hal.inria.fr/hal-03272963v1.

It contains also an **XP algorithm for bipartite independent set reconfiguration**. It may also
be used for **directed pathwidth**, thanks to the **equivalence** showed in https://hal.inria.fr/hal-03272963v1.
Its complexity is $`O(n^{2k+2.5})`$-time and $`O(n^{2})`$-space.

And, as a last algorithm that it contains, is the one for the Subtree method, its complexity `O(n^{2p+6})`$-time and $`O(n^{p})`$-space, with $`p`$ number of leafs in the tree in the RNA circle formalism.

# Additional content

- If you choose to weight the model through the configuration (isweighted = 1), it will use the routines that are inside the weighter.py in order to :
* Draw an RNA sequence at random (with no bias with the use of dnamic programming and of automatas) compatible with both secondary structure
*  Solve the problem with Subtree method
* Remove the intrication of secondary structures and multiples arcs in order to solve the problem with DPW and MCF.

- You can also launch directly the script in test\_weighted\_Turner in an additionnal benchmark utterly separated for the core of the benchmarks. It allow you to launch a unitary model against a weighted model and to compare them along real Turner energy model computation using RNAeval from the VIENNA package.

# Howto

Installing dependencies (pybind11, numpy, pytest, networkx) and setting up the environment:

    python3 -m pip install -r requirements.txt
    . ./setenv.sh 

Then, if you are on linux: 

    make

If you are on mac:

    make macos

Try a first tests to see if everything is setup :

    make test

If they all pass, you are good to go.

# Test design 

In order to do some tests, here are the possible commands :

	make check
It will launch all the tests in the test folder with the 3 methods with debug script for all of them. Furthermore, it returns performances for the different metrics, times and parameters values in a CSV file. (one where it was already averaged at some point: performancies.csv and the other with raw outputs: performancies_full.csv) 

	make checksilent
It works as makecheck but will not print any debug message.

	make fullcheck
Checks instances and returns also some coverage metrics for the code
(less relevant now due to the coexistence of the weighted and unweighted model in the same file and due to the use of C++ routines that are not "covered", but was useful in the first steps of developpement)

	make checkprofile|grep -p "interestingfile.py" 
That will gives the time in each Python function in the weighted_ordering file.


In order to parametrize the tests, the options use are the one in the function inside_configuration_tests.py, here are the options that could be changed and theirs meanings :

	* uw_[color] : The unpaired weight, the higher it is, the less positions would be matched in the corresponding colored RNA sequence.
	* lw_[color] : The leaf weight, the higher it is, the more the sequences would have chance to produce more leafs in the sense of the tree in the RNA circle formalism. 
	* theta_[color] in general, the minimal distance of a secondary structure arc. In term of parenthesis, the more theta is high, the more parentheses that matched will be far from each other in the sequence. Most of the time, theta_red = theta_red in {1, 3, 5}.
	* length_of_sequences : list of all the lengths of sequences to test, we will generate number_of_runs sequenes for each length.
	* number_of_runs = The number of instances generated at a fixed value of length of the sequence
	* isweighted : A boolean to say if you want to launch a weighted model or not for the 3 algorithms
	* isweighted : A boolean to say if you want to launch MCF method, we allow to disable it for large benchmark are it is often long for this method to converge.

# Known bugs and limitations

For the moment, to be allowed to launch weighted model even with DPW and MCF, you have to use weights values that are integers.

# Contributors

* Théo BOURY
* Laurent BULTEAU
* Bertrand MARCHAND
* Yann PONTY


