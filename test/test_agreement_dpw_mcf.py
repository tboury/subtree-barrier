import sys
import time
import multiprocessing
import matplotlib.pylab as plt


from rna_interface import random_conflict_graph, structures_to_conflict_graph
from dpw_interface import extend_to_pm
from maximum_matching import maximum_matching
from dpw_interface import construct_digraph
from barriers_py3 import realize
from dpw_algorithm import DPW
from graph_classes import Digraph

from weighted_ordering import main_subtree, init_tree_circle_formalism, count_leafs
from utilitary import build_local_stats, build_total_stats, initialize_sequences, rearrange_sequences
from configuration_tests import choose_parameter
from weighter import unfold_weight, give_weight, build_RNA
seed = 2021



def test_agreement_dpw_mcf_small_graphs():
    
    theta,uw,lw,theta2,uw2,lw2,ns,nruns, isweighted, mcfallowed = choose_parameter()

    partial_time = [{},{},{}]
    partial_time_full = [{},{},{}]

    for n in ns:
        time_dpw = 0
        time_mcf = 0
        time_subtree = 0
        for inc in range(nruns):
            #solve with subtree method
            schedule, k3, predecessor, wholeB, ss1, ss2, time_subtree, rev,counted_leafs, RNA_s = solve_subtree(n, seed+inc, theta=theta, uw=uw, lw=lw, additional = [uw2, theta2, lw2], isweighted = isweighted)
            print("time_subtree", time_subtree)

            #solve with dpw method
            k1, n_ur, time_dpw,ss1_stat, ss2_stat = solve_dpw(n, seed+inc, theta=theta, uw=uw, lw=lw, rev = rev, additional = [uw2, theta2, lw2], isweighted= isweighted,  RNA_s = RNA_s)
            print("time_dpw", time_dpw)
            #solve with mcf method 
            time_mcf=-1
            #mcf method can be removed for now as too long for large instances in configuration file
            if mcfallowed:
                k2, time_mcf = solve_mcf(n, seed+inc, theta=theta, uw=uw, lw=lw, rev = rev, additional = [uw2, theta2, lw2], isweighted = isweighted,  RNA_s = RNA_s)
                print("time_mcf", time_mcf)
            else:
                k2 = k1 + 1 - n_ur
            #Verify that no errors occured
            assert(k2==k3)
            assert(k1+1==k2+n_ur)
            assert(k1+1==k3+n_ur) #Framework was retest on 600 examples, ns = [5,10,15,20,25,30] and works

            #Build statistics for comparison of performances
            timerr = [time_dpw, time_mcf, time_subtree]
            (partial_time, partial_time_full) = build_local_stats(ss1_stat, ss2_stat, k3, theta, predecessor, timerr, n, partial_time, partial_time_full,counted_leafs)
            print(" ")
            
    build_total_stats(partial_time, partial_time_full)
    


def test_small_instance1():
    
    n = 20
    schedule, k3, nb_leafs, wholeB, ss1, ss2, time_subtree, rev, counted_leafs, RNA_s = solve_subtree(n, seed)
    k1, n_ur, time_dpw,ss1_stat, ss2_stat = solve_dpw(n, seed, rev=rev)
    k2, time_mcf = solve_mcf(n, seed, rev=rev)

    assert(k1+1==k2+n_ur)
    assert(k1+1==k3+n_ur)

      
def test_small_instance2():

    n = 20
    schedule, k3, nb_leafs, wholeB, ss1, ss2, time_subtree, rev, counted_leafs, RNA_s = solve_subtree(n, seed + 1)  
    k1, n_ur, time_dpw,ss1_stat, ss2_stat = solve_dpw(n, seed + 1, rev=rev)
    k2, time_mcf = solve_mcf(n, seed + 1, rev=rev)

    assert(k1+1 == k3+n_ur)
    assert(k1+1==k2+n_ur)
    
def test_small_instance3():

    n = 20
    schedule, k3, nb_leafs, wholeB, ss1, ss2, time_subtree, rev, counted_leafs, RNA_s = solve_subtree(n, seed + 62)
    k1, n_ur, time_dpw,ss1_stat, ss2_stat = solve_dpw(n, seed + 62, rev=rev)
    k2, time_mcf = solve_mcf(n, seed + 62, rev=rev)
    
    assert(k1+1 == k3 + n_ur)
    assert(k1+1==k2+n_ur)

def test_small_instance4():

    n = 10
    schedule, k3, nb_leafs, wholeB, ss1, ss2, time_subtree, rev,counted_leafs, RNA_s = solve_subtree(n, seed)
    k1, n_ur, time_dpw,ss1_stat, ss2_stat = solve_dpw(n, seed, rev=rev)
    k2, time_mcf = solve_mcf(n, seed, rev=rev)
    
    assert(k1+1 == k3+n_ur)
    assert(k1+1==k2+n_ur)
    


def test_small_instance5():
    
    s1 ="((((..))))"
    s2 ="(((....)))"
    n=10
    
    # get conflict graph
    G = structures_to_conflict_graph(s1, s2)

    # solve dpw
    k1, n_ur, time_dpw  = solve_graph_dpw(G,n)

    # solve mcf
    k2,time_mcf = solve_struct_mcf(s1, s2)
    
    print("dpw: k1, n_ur", k1, n_ur)
    print("mcf: k2", k2)
    assert(k1+1==k2+n_ur)

def test_small_instance6():
    

    s1 =".((...(((.)))))"
    s2 ="((((.)((.).))))"
    n=15
    
    # get conflict graph
    G = structures_to_conflict_graph(s1, s2)

    # solve dpw
    k1, n_ur, time_dpw = solve_graph_dpw(G,n)

    # solve mcf
    k2,time_mcf = solve_struct_mcf(s1, s2)
    
    #solve subtree
    (schedule, k3, nb_leafs, wholeB) = main_subtree(s1,s2, totest=1) 
    
    print("dpw: k1, n_ur", k1, n_ur)
    print("mcf: k2", k2)
    print("subtree: k3", k3)
    assert(k1+1==k3+n_ur)
    assert(k1+1==k2+n_ur)

def test_small_instance7():
    
    s1 = '.(.(...)).'
    s2 = '(..(...).)'
    n=10
    
    # get conflict graph
    G = structures_to_conflict_graph(s1, s2)

    # solve dpw
    k1, n_ur, time_dpw = solve_graph_dpw(G,n)

    # solve mcf
    k2,time_mcf = solve_struct_mcf(s1, s2)
    
    #solve subtree
    (schedule, k3, nb_leafs, wholeB) = main_subtree(s1,s2, totest=1) 
    
    print("dpw: k1, n_ur", k1, n_ur)
    print("mcf: k2", k2)
    print("subtree: k3", k3)
    assert(k1+1==k3+n_ur)
    assert(k1+1==k2+n_ur)


def test_small_instance8():
    
    s1= "........()"
    s2= "(...())..."
    n=10
    
    # get conflict graph
    G = structures_to_conflict_graph(s1, s2)#

    # solve dpw
    k1, n_ur, time_dpw  = solve_graph_dpw(G,n)

    # solve mcf
    k2,time_mcf = solve_struct_mcf(s1, s2)
    
    print("dpw: k1, n_ur", k1, n_ur)
    print("mcf: k2", k2)
    assert(k1+1==k2+n_ur) 

def test_small_instance9():
    
    s1="(.((.......................(.((..(.(...))....)))..))......................)..."
    s2=".(..(((((((((((((((((((((((.(..((.(.(((..))))...))..)))))))))))))))))))))).)))"
    n=len(s1)
    # get conflict graph
    G = structures_to_conflict_graph(s1, s2)#

    # solve dpw
    k1, n_ur, time_dpw  = solve_graph_dpw(G,n)

    # solve mcf
    k2,time_mcf = solve_struct_mcf(s1, s2)
    
    print("dpw: k1, n_ur", k1, n_ur)
    print("mcf: k2", k2)
    assert(k1+1==k2+n_ur) 

def solve_graph_dpw(G,n):

    if len(G.left)==0:
        return -1, 0, 0

    M = maximum_matching(G)
    
    M_dict = {}

    for u,v in M:
        M_dict[u] = v
        M_dict[v] = u

    n_ur = 0

    for u in G.right: 
        if u not in M_dict.keys():
            n_ur += 1 
    
    G, M = extend_to_pm(G,M)
    
    M_dict = {}

    for u,v in M:
        M_dict[u] = v
        M_dict[v] = u


    H, int_to_e, e_to_int = construct_digraph(G.left, G.right, G, M_dict)     
 
    k = 0
    dpw = time.perf_counter() 

    while k <= n:
    
        seq = DPW(H, k, break_into_scc=True)      
 
        if seq is not None:
            break

        k+= 1

    dpw = - dpw + time.perf_counter() 
    return k, n_ur, dpw 


def solve_dpw(n, seed, theta=1, uw=0.2, lw=1., rev = 0, additional = [], isweighted = 0,  RNA_s = ""):

    (s1,s2,ss1,ss2,bps1,bps2) = initialize_sequences(n, seed, theta, uw, lw, rev, additional, count_fixed = 1)
    print("s1_dpw", s1)
    print("s2_dpw", s2)
    if isweighted:
        (s1,s2,ss1,ss2,bps1,bps2) = unfold_weight(s1, s2, RNA_s,ss1,ss2)
        print("DPW : RNA_s, s1, s2", RNA_s, s1, s2)    
    dpw= time.perf_counter() 
    
    G = structures_to_conflict_graph(s1, s2) 
    n = len(s1)
    k, n_ur, dd = solve_graph_dpw(G,n)

    dpw = - dpw + time.perf_counter() 

    return k, n_ur, dpw, ss1, ss2

def solve_mcf(n, seed, theta=1, uw=0.2, lw=1., rev = 0, additional = [], isweighted = 0,  RNA_s = ""):


    (s1,s2,ss1,ss2,bps1,bps2) = initialize_sequences(n, seed, theta, uw, lw, rev, additional, count_fixed = 1)

    #print("s1_mcf", s1)
    #print("s2_mcf", s2)
    if isweighted:
        (s1,s2,ss1,ss2,bps1,bps2) = unfold_weight(s1, s2, RNA_s,ss1,ss2)
        print("MCF : RNA_s, s1, s2", RNA_s, s1, s2)   
    k = 0
    n = len(s1)
    mcf = time.perf_counter() 
    
    while True and k < n:
        
        p = realize(ss1, ss2, k, 1, theta=theta)
        print("k", k)
        if p is not None:
            break
        k += 1

    mcf = - mcf + time.perf_counter() 
    
    return k, mcf

def solve_struct_mcf(s1, s2):
    
    n = len(s1)
    (s1,s2,ss1,ss2,bps1,bps2) = rearrange_sequences(s1, s2, 0)
    
    #print("s1_struct_mcf", s1)
    #print("s2_struct_mcf", s2)
    
    k = 0
    
    mcf = time.perf_counter() 
    
    while True and k < n:
        
        p = realize(ss1, ss2, k, 1, theta=1)
        if p is not None:
            break
        k += 1

    mcf = - mcf + time.perf_counter() 
    
    return k, mcf


def helper_multiprocess_ordering(i, q, s1, s2, weight1, weight2):
    timer = time.perf_counter() 
    if i == 1:
        (schedule, k3, predecessor, wholeB) = main_subtree(s2,s1, totest=1, weight = weight2)
    else:
        (schedule, k3, predecessor, wholeB) = main_subtree(s1,s2, totest=1, weight = weight1)
    timer = time.perf_counter()  - timer
    rev = i
    q.put((timer, schedule, k3, predecessor, wholeB, rev))


def solve_subtree(n, seed, theta=1, uw=0.2, lw=1., additional = [], isweighted = 0):


    (s1,s2,ss1,ss2,bps1,bps2) = initialize_sequences(n, seed, theta, uw, lw, 0, additional, count_fixed = 1)
    
    print("s1_subtree", s1)
    print("s2_subtree", s2)
    print(ss1, ss2)
    if isweighted:
        RNA_s = build_RNA(ss1,ss2)
        weight1 = give_weight(s1, s2, RNA_s, bps1, bps2)
        weight2 = give_weight(s2, s1, RNA_s, bps2, bps1)
    else:
        RNA_s = []
        weight1 = []
        weight2 = []
    print("RNA_s", RNA_s, "weight1", weight1, "weight2", weight2)
    
    #Run both try from blue to red and red to blue in parallel and stops when the first one ends
    
    subtree2 = time.perf_counter() #time.time()
    all_process = []
    q = multiprocessing.Queue()
    
    for i in range(0, 2):
        process = multiprocessing.Process(target=helper_multiprocess_ordering, args=(i,q,s1,s2, weight1, weight2))
        process.start()
        all_process.append(process)
        
    while all_process[0].is_alive() and all_process[1].is_alive():
        pass
    subtree2 = - subtree2 + time.perf_counter() 
    
    (subtree, schedule, k3, predecessor, wholeB, rev) = q.get()
    
    for process in all_process:
        if process.is_alive():
            process.terminate()
            
    #The time is taken here too avoid any overhead due to multiprocessing that is not optimized here
    subtree=min(subtree,subtree2)
    if rev:
        counted_leafs = count_leafs(init_tree_circle_formalism(s1))
    else:
        counted_leafs = count_leafs(init_tree_circle_formalism(s2))
    #FRAMEWORK WITH EMULATION OF PARALLELIZATION BELOW
    #(schedule, k3, predecessor, wholeB, rev) = main_subtree(s1,s2, totest=1, decision=1)
    #subtree2 = time.time()
    #(schedule2, k32, predecessor2, wholeB2, rev2) = main_subtree(s2,s1, totest=1, reverse=1, decision=1)
    #subtree2 = - subtree2 + time.time()
    #if subtree2 < subtree:
    #(schedule, k3, predecessor, wholeB, rev) = (schedule2, k32, predecessor2, wholeB2, rev2)
    #subtree = subtree2
    return (schedule, k3, predecessor, wholeB, ss1, ss2, subtree, rev, counted_leafs,RNA_s)


if __name__=='__main__':
    
    test_agreement_dpw_mcf_small_graphs()
    
    #test_small_instance1()
    #test_small_instance2()
    #test_small_instance3()
    #test_small_instance4()
    #test_small_instance5()
    #test_small_instance6()
    #test_small_instance7()
    #test_small_instance8()
    #test_small_instance9()

