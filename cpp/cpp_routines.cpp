#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <map> 
#include <list> 
#include <utility> 
#include <iostream> 
#include <algorithm>
using namespace std;

int LARGE_VALUE = 1 << 20;

bool BFS_hopcroft_karp(map<int, list<int>> &ngbh,
                       list<int> &left,
                       list<int> &right,
                       map<int, int> &match_of_left,
                       map<int, int> &match_of_right,
                       map<int, int> &distances)
{
    

    list<int> queue;

    for (auto const & u : left)
    {
        if (match_of_left[u]==-1)
        {
            distances[u] = 0;
            queue.push_back(u);
        }
        else
        {
            distances[u] = LARGE_VALUE;
        }
    }
    distances[-1] = LARGE_VALUE;

    while (queue.size() > 0)
    {
        int u = queue.front();
        queue.pop_front();
        if (distances[u] < distances[-1])
        {
            for (auto const & v : ngbh[u])
            {
                if (distances[match_of_right[v]] == LARGE_VALUE)
                {
                    distances[match_of_right[v]] = distances[u] + 1;
                    queue.push_back(match_of_right[v]);
                }
            }
        }
    }

    if (distances[-1] < LARGE_VALUE)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool DFS_hopcroft_karp(int u,
                       map<int, list<int>> ngbh,
                       map<int, int> &distances,
                       map<int, int> &match_of_right,
                       map<int, int> &match_of_left)

{
    if (u >= 0)
    {
        for (auto const & v : ngbh[u])
        {
            if (distances[match_of_right[v]] == distances[u] + 1)
            {
                if (DFS_hopcroft_karp(match_of_right[v],
                                      ngbh,
                                      distances,
                                      match_of_right,
                                      match_of_left))
                {
                    match_of_right[v] = u;
                    match_of_left[u] = v;
                    return true;
                }
            }
        }
        distances[u] = LARGE_VALUE;
        return false;
    }
    return true;
}

list<pair<int, int>> hopcroft_karp(map<int, list<int>> ngbh,   // adjacency
                                   list<int> left,             // one side
                                   list<int> right)            // other side
{

    map<int, int> match_of_left;  // keys: left, values: the match in right
    map<int, int> match_of_right; // keys: right, values: the match in left

    for (auto const& u : left) match_of_left[u] = -1; // init as unmatched
    for (auto const& u : right) match_of_right[u] = -1; 

    map<int, int> distances;

    bool keep_going = BFS_hopcroft_karp(ngbh, 
                                        left,
                                        right,
                                        match_of_left,
                                        match_of_right,
                                        distances);

    // main loop
    while (keep_going)
    {
        for (auto const& u : left)
        {
            if (match_of_left[u] == -1) 
            {
                DFS_hopcroft_karp(u,
                                  ngbh,
                                  distances,
                                  match_of_right,
                                  match_of_left);
            }            
        }
        keep_going = BFS_hopcroft_karp(ngbh, 
                                       left,
                                       right,
                                       match_of_left,
                                       match_of_right,
                                       distances);
    }
    
    list<pair<int,int>> matching; // the object that will be returned

    for (auto const & u : left)
    {
        if (match_of_left[u] != -1)
        {
            pair<int, int> match_edge;
            match_edge.first = u;
            match_edge.second = match_of_left[u];
            matching.push_back(match_edge);
        } 
    }


    return matching;
}

int add(int i, int j) {
    return i + j;
}

void DFS_visit(map<int, list<int>> out_ngbh,
               int u,
               list<int> &container,
               map<int, int> &color)
{
    color[u] = 1; 
    for (auto const& v : out_ngbh[u])
    {
        if (color[v] == 0) 
        {
            DFS_visit(out_ngbh, 
                      v,
                      container,
                      color);
        }
    }
    container.push_back(u);
}

void DFS(map<int, list<int>> out_ngbh,
         list<int> &finishing_order,
         list<list<int>> &SCC,
         bool transpose=false)
{
    // In both cases, start by coloring all vertives white
    map<int, int> color;

    for (auto const& pair : out_ngbh)
    {
        color[pair.first] = 0; // white
    }

    // If we are in the first, forward "finishing_times computing" case:
    if (!transpose)
    {
        for (auto const& pair : out_ngbh)
        {
            if (color[pair.first] == 0) 
            {
                DFS_visit(out_ngbh, 
                          pair.first, 
                          finishing_order,
                          color);
            }
        }
    }
    // Else we are in the "reverse, finishing_order-based SCC computing case"
    else
    {
        list<int>::reverse_iterator rev_iter;
        for (rev_iter = finishing_order.rbegin(); 
             rev_iter != finishing_order.rend();
             rev_iter++)
        {

            list<int> scc;

            if (color[*rev_iter] == 0) 
            {
                DFS_visit(out_ngbh, 
                          *rev_iter, 
                          scc,
                          color);
            }
   
            if (scc.size() > 0) SCC.push_back(scc);
        }

    }
}



list<list<int>> strongly_connected_components(map<int, list<int>> out_ngbh,
                                   map<int, list<int>> in_ngbh)
{
    
    list<int> finishing_order;
    list<list<int>> SCC; 

    // first call: computes finishing_order, SCC untouched.
    DFS(out_ngbh, finishing_order, SCC); 

    // second call: uses finishing_order to iterate, populate SCC
    DFS(in_ngbh, finishing_order, SCC, true);

    // The SCCs are returned in a topological order.
    return SCC;
}

int includer(list<int> gauche, list<int> droit)
{
    if (includes( droit.begin(), droit.end(), gauche.begin(), gauche.end()))
        return 1;
    return 0;
}

int ispos(list<int> B, list<int> R, list<int> weight)
{   
    //Check if block associated with set B is positive knowing the weight function
    int resu =  0;
    list<int>::iterator it_loc ;
    if (weight.begin() != weight.end())
    {
        for (list<int>::iterator it=R.begin(); it !=R.end(); ++it)
        {
            it_loc = weight.begin();
            advance(it_loc, (*it));
            resu += (*it_loc);
        }
        for (list<int>::iterator it=B.begin(); it !=B.end(); ++it)
        {
            it_loc = weight.begin();
            advance(it_loc, (*it));
            resu -= (*it_loc);
        }
    }
    else
    {
        resu = R.size() - B.size();
    }
    return (resu >= 0);
}

list<int> computer_N_star(list<int> list_blues,list<list<int>> Adjacency)
{
    //Compute the strict neighborhood of list_blues
    list <int> red_candidates;
    list<list<int>>::iterator it;
    //First, we check the red that can possibly be in thestrict neighborhood
    for (list<int>::iterator blue= list_blues.begin(); blue != list_blues.end() ; ++blue)
    {
        it = Adjacency.begin();
        advance(it, (*blue) );
        list<int> dest;
        set_union(red_candidates.begin(), red_candidates.end(), (*it).begin(),(*it).end(), back_inserter(dest) );
        red_candidates = dest;

    }

    list <int> resu;
    //We now verify is these red are reallys strict or are just simple neighborhood
    for (list<int>::iterator red= red_candidates.begin(); red != red_candidates.end() ; ++red)
    {
        it = Adjacency.begin();
        advance(it, (*red));
        if (includes( list_blues.begin(), list_blues.end(), (*it).begin(), (*it).end()))
        {
            resu.push_back((*red));
        }
    }
    return resu;
}

tuple <list <int>, list<int>, int> detecter(list<int> B, list<int> sigma, list<list<int>> Adjacency, int K, list<tuple<list<int>, int>> fill_matrix, list<int> weight)
{
    //Detect the SUSs
    list<list<int>>::iterator it = Adjacency.begin();
    list<int>::iterator it_loc = (*it).begin();
    int nb_blues = (*it_loc);
    tuple<list<int>, int> tu;
    //Use the sort in size
    for (list <tuple<list<int>, int>>::iterator rank = fill_matrix.begin(); rank !=  fill_matrix.end(); ++rank)
    {
        
        tu = (*rank);
        //We check first condition that are the more likely to be false to be as fast as possible in practice 
        if (get<1>(tu) <= K && get<1>(tu) != -1)
        {
            list <int> subtree;
            list<int> lili = get<0>(tu);
            for (list <int>::iterator bl = lili.begin(); bl != lili.end(); ++bl)
            {
                if ((*bl) <= nb_blues)
                {
                    subtree.push_back((*bl));
                }
            }
        
            if (subtree.begin() != subtree.end())
            {

                subtree.sort();
                if (ispos(subtree, computer_N_star(subtree, Adjacency), weight))
                {
                    if (includer(subtree, B))
                    {

                        return {subtree, lili, 1};
                    }
                }

            }
        }
    }
    list <int> empt;
    return {empt, empt, -1};
}



list<int> difference(list<int> li1,  list<int> li2)
{
    list<int> dest;
    set_difference(li1.begin(), li1.end(), li2.begin(),li2.end(), back_inserter(dest) );
    return dest;
}

list<int> adder(list<int> li1,  list<int> li2)
{
    list<int> dest;
    set_union(li1.begin(), li1.end(), li2.begin(),li2.end(), back_inserter(dest) );
    return dest;
}

int barrier(list<int> schedule, list<int> b)
{
    int barrier = 0;
    int current = 0;
    list<int>::iterator it;
    for (list<int>::iterator task=schedule.begin(); task !=schedule.end(); ++task)
    {
        it = find(b.begin(), b.end(), *task);
        if(it != b.end())
        {
            current += 1;
        }
        else
        {
            current -= 1;
        }
        barrier = max(barrier, current);
    }
    return barrier;

}

int barrier_w(list<int> schedule, list<int> b, list<int> weight)
{
    int barrier = 0;
    int current = 0;
    list<int>::iterator it;
    list<int>::iterator it_w;
    for (list<int>::iterator task=schedule.begin(); task !=schedule.end(); ++task)
    {
        it = find(b.begin(), b.end(), *task);
        it_w = weight.begin();
        advance(it_w, (*task));
        if(it != b.end())
        {
            current += (*it_w);
        }
        else
        {
            current -= (*it_w);
        }
        barrier = max(barrier, current);
    }
    return barrier;

}

list<list<int>> reducer_adjacency(list<list<int>> Adjacency, list<int> tree, list<int> N_star)
{
    //A function to reduce adjacency matrix for subtrees
    list<list<int>> resu;
    list<list<int>>::iterator it = Adjacency.begin();
    list<int>::iterator itfind;
    resu.push_back((*it));
    list<int>::iterator it_loc = (*it).begin();
    int nb_blues = (*it_loc);
    advance(it_loc, 1);
    int nb_reds = (*it_loc);
    //First, we reduce the adjacency of the blue nodes/BPs 
    for (int blue = 1; blue != 1 + nb_blues ; ++blue)
    {
        it = Adjacency.begin();
        advance(it, blue);
        list<int> resu_loc;
        itfind = find(tree.begin(), tree.end(), blue);
        if (itfind != tree.end())
        {
               for (list<int>::iterator red= (*it).begin(); red != (*it).end() ; ++red)
                {
                    itfind = find(N_star.begin(), N_star.end(), (*red));
                    if (itfind != N_star.end())
                    {
                        resu_loc.push_back((*red));
                    }
                }
        }
        resu.push_back(resu_loc);
    }
    //Next, we reduce the adjacency of the red nodes/BPs
    for (int red = 1 + nb_blues; red != nb_blues + nb_reds + 1 ; ++red)
    {
        it = Adjacency.begin();
        advance(it, red);
        list<int> resu_loc;
        itfind = find(N_star.begin(), N_star.end(), red);
        if (itfind != N_star.end())
        {
               for (list<int>::iterator blue= (*it).begin(); blue != (*it).end() ; ++blue)
                {
                    itfind = find(tree.begin(), tree.end(), (*blue));
                    if (itfind != tree.end())
                    {
                        resu_loc.push_back((*blue));
                    }
                }
        }
        resu.push_back(resu_loc);
    }

    return resu;
}


PYBIND11_MODULE(cpp_routines, m) {
    m.doc() = "pybind11 example plugin"; // optional module docstring

    m.def("add", &add, "A function which adds two numbers");

    m.def("strongly_connected_components", 
          &strongly_connected_components, 
          "SCC");
    m.def("includer", &includer, "compute the inclusion of one list into the other");
    m.def("reducer_adjacency", & reducer_adjacency, "reduce adjacency matrix");
    m.def("computer_N_star", &computer_N_star, "compute N star");
    m.def("adder", &adder, "add sorted list with no doublons");
    m.def("difference", &difference, "difference between sorted list by removing second one from first one");
    m.def("barrier", &barrier, "compute simple barrier");
    m.def("detecter", &detecter, "detecter of Safe Unitary Sets");
    m.def("barrier_w", &barrier_w, "compute barrier with weights");
    m.def("hopcroft_karp",
          &hopcroft_karp,
          "maximum matching algorithm");
}
