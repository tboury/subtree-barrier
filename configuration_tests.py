def choose_parameter():
    
    #theta_blue=3 #For high values of lenth_sequences
    #uw_blue = 4.3 
    #lw_blue = 0.001
    #theta_red = 3 
    #uw_red = 0.05
    #lw_red = 10. 
    
    #theta_blue=1 #For small values of lenth_sequences
    #uw_blue = 0.2 
    #lw_blue = 0.1 
    #theta_red = 1 
    #uw_red = 0.8
    #lw_red = 0.5 
    
    theta_blue=3  #These parameters + lenght_sequences= 100 => DPW blow up
    uw_blue=5. 
    lw_blue = 7. 
    theta_red = 3
    uw_red=7.
    lw_red=10
    
    #length_sequences = [30]
    #length_sequences = [50] 
    length_sequences = [70] 
    #length_sequences = [100]
    #length_sequences = [5, 10, 15, 20, 25, 30]
    #length_sequences = [5, 10, 15, 20, 25, 30, 35, 40]
    #length_sequences = [10, 15, 20, 25, 30, 35 , 40, 45, 50]
    #number_of_runs=1
    #number_of_runs = 5
    #number_of_runs = 10
    #number_of_runs = 30
    number_of_runs = 100
    
    isweighted=0
    mcfallowed=1
    
    
    return theta_blue,uw_blue,lw_blue,theta_red,uw_red,lw_red,length_sequences,number_of_runs,isweighted,mcfallowed
